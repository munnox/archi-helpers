#!/bin/env python
# import json
from typing import Any, Dict, Type, List
import click
import traceback, sys
import logging

from archi.models import (
    ArchiError,
    Archimodel,
    Meta,
    Folder,
    Node,
    Link,
    Profile,
    Property,
)
from archi.models import process_element
import archi.storage


# logger = logging.getLogger("main").setLevel(logging.INFO)
# logging.getLogger("matplotlib").setLevel(logging.INFO)
logger = logging.getLogger(__name__)


def convert_to_dict(elements):
    if type(elements) == type([]):
        return [e.to_dict() for e in elements]
    elif type(elements) == type({}):
        # logger.info(f"convert to dict: %s", elements)
        return [elements[k].to_dict() for k in elements]
    raise ArchiError("Should not get here. Fail noisy!!!")


def process_dict(model):
    try:
        main_model = model["archimate:model"]
    except KeyError as error:
        click.echo(f"No archimate:model stucture to process.")
    folders = {f["@name"]: f for f in main_model["folder"]}
    click.echo(f"Main folders found: {folders.keys()}")
    # TODO Need to extracts profiles for specialisations
    archimodel = Archimodel()
    meta = Meta()
    meta.path = []
    archimodel.profiles = {
        e["@id"]: process_element(meta, e) for e in main_model.get("profile", [])
    }
    # Process all folders placing the elements under the different areas:
    # links - relations
    # views - Views
    # elements - all others
    for name in folders:
        folder = folders[name]
        meta.path = [name]
        processed_folder = process_element(meta, folder)
        subelements = processed_folder.SubElements
        if "Relations" == name:
            archimodel.links.extend(subelements.values())
        elif "Views" == name:
            archimodel.views.extend(subelements.values())
        else:
            archimodel.elements.extend(subelements.values())

        subelements_info = [{"name": se.Name} for se in subelements.values()]
        logger.info(
            f"Main Level Folder name: '{name}' number of element: {len(subelements)}"
        )  # , Type: {types}")
        # logger.info(f"Main Level Folder Elements: {subelements.keys()}")
        types = [e.Type for e in subelements.values()]
        archimodel.folders[name] = types

    json_model = {
        "nodes": [
            {
                "ID": main_model["@id"],
                "Type": "ArchimateModel",
                "Name": main_model["@name"],
                "Documentation": main_model.get("purpose", None),
                "Specialization": "",
            }
        ],
        "links": convert_to_dict(archimodel.links),
        "views": convert_to_dict(archimodel.views),
        "folder_layout": archimodel.folders,
        "profiles": convert_to_dict(archimodel.profiles),
    }
    json_model["nodes"].extend(convert_to_dict(archimodel.elements))

    return json_model


@click.command()
@click.argument("model_file", type=click.Path(exists=True))
@click.argument("yaml_file", type=click.Path())
# @click.argument("output_file", type=click.Path())
def main(model_file, yaml_file):  # , output_file):
    logging.basicConfig(
        format="%(asctime)s|%(levelname)s|%(name)s|%(funcName)s:%(lineno)d|>%(message)s",
        level=logging.DEBUG,
        datefmt="%H:%M:%S",
        stream=sys.stderr,
    )
    try:
        archi_dict_model = archi.storage.read_archi_file(model_file)
        dict_data = archi.storage.dump_yaml("archi.yaml", archi_dict_model)
        archi.storage.write_xml_file("archi.xml", archi_dict_model)

        dict_model = process_dict(archi_dict_model)
        click.echo(f"Archi model file read")
        # dump_json(json_file, dict_model)
        archi.storage.dump_yaml(yaml_file, dict_model)
    except Exception as error:
        click.echo(f"Error: {type(error)} - {error}")
        click.echo(f"format_exc: {traceback.format_exc()}")


if __name__ == "__main__":
    main()
