import click
from archi.converters import convert_csv_export_to_json


@click.command()
@click.argument("nodes_file", type=click.Path(exists=True))
@click.argument("properties_file", type=click.Path(exists=True))
@click.argument("links_file", type=click.Path(exists=True))
@click.argument("output_file", type=click.Path())
def main(nodes_file, properties_file, links_file, output_file):
    convert_csv_export_to_json(nodes_file, properties_file, links_file, output_file)
    click.echo(f"Successfully converted {nodes_file} and {links_file} to {output_file}")


if __name__ == "__main__":
    main()
