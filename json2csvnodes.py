import click
import csv
import json

from archi.converters import read_json, convert_to_csv


@click.command()
@click.argument("json_file", type=click.Path(exists=True))
@click.argument("nodes_file", type=click.Path())
@click.argument("links_file", type=click.Path())
def main(json_file, nodes_file, links_file):
    data = read_json(json_file)
    nodes = data["nodes"]
    links = data["links"]

    # Write nodes to CSV file
    convert_to_csv(nodes_file, nodes)
    # with open(nodes_file, "w") as f:
    #     writer = csv.DictWriter(f, fieldnames=nodes[0].keys())
    #     writer.writeheader()
    #     writer.writerows(nodes)

    # Write links to CSV file
    convert_to_csv(links_file, links)
    # with open(links_file, "w") as f:
    #     writer = csv.DictWriter(f, fieldnames=links[0].keys())
    #     writer.writeheader()
    #     writer.writerows(links)

    click.echo(f"Successfully converted {json_file} to {nodes_file} and {links_file}")


if __name__ == "__main__":
    main()
