# Setup the local env with user

ARG ARCHI_IMAGE_NAME=archi-helpers-archi:0.1.0
# GET Archi from Archi Image
FROM ${ARCHI_IMAGE_NAME} AS archi

# Build Web image from Python Base
FROM python:3.11 AS explorebase

ENV RUN_ENV=self
ENV DJANGO_SUPERUSER_USERNAME=admin
ENV DJANGO_SUPERUSER_EMAIL=a@b.com
ENV DJANGO_SUPERUSER_PASSWORD=password

# Archi Running Prereqs
RUN apt update && \
    apt install -y xvfb libswt-gtk-4-java && \
    apt autoremove -y && apt clean

COPY --from=archi /opt/Archi/ /opt/Archi/
# Pulling useful scripts from Archi Image
COPY --from=archi /usr/local/bin/archi_certs /usr/local/bin
COPY --from=archi /usr/local/bin/archi_cli /usr/local/bin
COPY --from=archi /usr/local/bin/archi_conv_csv /usr/local/bin
COPY --from=archi /usr/local/bin/archi_conv_report /usr/local/bin
ENV PATH=/opt/Archi/:$PATH

# # Nodejs Install from https://github.com/nodejs/docker-node/blob/main/18/bullseye/Dockerfile
COPY --from=node:18 /usr/local/ /usr/local/nodejs
# COPY --from=node /usr/local/bin/node /usr/local/bin/node
# COPY --from=node /usr/local/bin/nodejs /usr/local/bin/nodejs
COPY --from=node:18 /opt/ /opt/yarn/

# Python poetry install
RUN pip install --upgrade pip && pip install poetry==1.8
RUN poetry config virtualenvs.create false

WORKDIR /src

COPY pyproject.toml /src/
COPY poetry.lock /src/

# RUN poetry install --no-interaction --no-ansi --only mai

# ========== Create a Unprivelaged web server =============

FROM explorebase AS explore-unpriv

ENV UID=200
ENV GID=200
ENV UNAME=django
# ENV RUN_ENV=self

RUN groupadd --gid $GID $UNAME \
  && useradd --uid $UID --gid $UNAME \
  --shell /bin/bash --create-home $UNAME

VOLUME ["/data"]
RUN mkdir /data/
RUN chown $UNAME:$UNAME -R /data

COPY --chown=$UNAME:$UNAME ./ /src/
COPY --chown=$UNAME:$UNAME containers/web/entry.sh /usr/local/bin/
# COPY --chown=$UNAME:$UNAME containers/web/conv_csv.sh /usr/local/bin/
# COPY --chown=$UNAME:$UNAME containers/web/conv_report.sh /usr/local/bin/
# RUN chown $UNAME:$UNAME -R /src

# RUN poetry install $(test "$RUN_ENV" == "production" && echo "--no-dev")
RUN poetry install --no-interaction --no-ansi $(bash -c 'test "$RUN_ENV" == "production" && echo "--only main"')

# USER $UNAME

EXPOSE 8000

# CMD ["/bin/sh", "/usr/local/bin/entry.sh"]
CMD ["/usr/local/bin/entry.sh"]
# ENTRYPOINT [ "/bin/sh", '-c' ]

# FROM explorebase as explore
# # ENV RUN_ENV=development
# COPY ./ /src/
# COPY containers/web/entry.sh /usr/local/bin/
# RUN poetry install --no-interaction --no-ansi $(bash -c 'test "$RUN_ENV" == "production" && echo "--only main"')
# # RUN echo $RUN_ENV $(bash -c 'test "$RUN_ENV" == "production" && echo "--only main"')
# CMD ["/bin/bash"]
# # CMD ["/bin/sh", "/usr/local/bin/entry.sh"]