#!/bin/sh
# From explore-django repo
# poetry run python manage.py migrate
# poetry run python manage.py runserver_plus --cert-file /tmp/cert.crt 0.0.0.0:8000
CERT_FILE=${CERT_FILE:='/tmp/cert.crt'}
# KEY_FILE=${KEY_FILE:='/tmp/cert.crt'}

echo "Migrating DB"

# From https://github.com/neo4j-contrib/django-neomodel/blob/master/tests/docker-entrypoint.sh
python -c <<EOF |
from django.db import IntegrityError
try:
  python manage.py install_labels
except IntegrityError:
  print("Already installed")
EOF

web migrate

echo "Creating Superuser"
web createsuperuser --noinput

# From https://github.com/neo4j-contrib/django-neomodel/blob/master/tests/docker-entrypoint.sh
# if [ "$DJANGO_SUPERUSER_USERNAME" ]
# then
#     python manage.py createsuperuser \
#         --noinput \
#         --username $DJANGO_SUPERUSER_USERNAME \
#         --email $DJANGO_SUPERUSER_EMAIL || true
# else
#     echo "No superusername found"
# fi

echo "Collecting Static files"
# Collect static files
web collectstatic --noinput



echo "Run server Verison: '$VERSION' in DEBUG: '$DEBUG' mode in env='$RUN_ENV' with key='$KEY_FILE' and cert='$CERT_FILE' "

case "$RUN_ENV" in

    self)
        echo "Development environment"
        web runserver_plus \
            --cert-file $CERT_FILE \
            0.0.0.0:8000
        echo "Server has quit"
        ;;
    development)
        echo "Development environment"
        # uwsgi --http :8000 --module app.wsgi
        # gunicorn --bind :8000 --chdir /src/ app.wsgi
        gunicorn \
            --bind :8000 \
            --keyfile $KEY_FILE \
            --certfile $CERT_FILE \
            --ssl-version TLSv1_2 \
            archihub.wsgi:application
        # explore runserver_plus \
        #     --cert-file $CERT_FILE \
        #     --key-file $KEY_FILE \
        #     0.0.0.0:8000
        ;;
    production)
        echo "Production environment"
        gunicorn \
            --bind :8000 \
            --keyfile $KEY_FILE \
            --certfile $CERT_FILE \
            --ssl-version TLSv1_2 \
            archihub.wsgi:application
        # explore runserver_plus \
        #     --cert-file $CERT_FILE \
        #     --key-file $KEY_FILE \
        #     0.0.0.0:8000
        ;;
    *)
        echo "Unknown Environment $RUN_ENV"
esac
