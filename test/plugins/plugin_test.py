from archi.pluginmanager import PluginBase

def test_filter(value):
    return f"Hello from TestPlugin value: {value}"

class TestPlugin(PluginBase):
    def run(self, *args, **kwargs):
        environment = args[0]
        print(f"Hello from HelloPlugin! Given: {args} -> {environment} going to add a test filter...")
        environment.filters["test_filter"] = test_filter