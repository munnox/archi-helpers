import click
import csv
import json

from archi.converters import nodeprops_to_json


@click.command()
@click.argument("nodes_file", type=click.Path(exists=True))
@click.argument("links_file", type=click.Path(exists=True))
@click.argument("output_file", type=click.Path())
def main(nodes_file, links_file, output_file):
    nodeprops_to_json(nodes_file, links_file, output_file)
    click.echo(f"Successfully converted {nodes_file} and {links_file} to {output_file}")


# def add_links_to_nodes(nodes, links):
#     node_dict = {node["ID"]: node for node in nodes}
#     for link in links:
#         source = node_dict[link["Source"]]
#         target = node_dict[link["Target"]]
#         if "Links" not in source:
#             source["Links"] = []
#         if "Links" not in target:
#             target["Links"] = []
#         source["Links"].append(link)
#         target["Links"].append(link)


# def read_csv(filepath):
#     with open(filepath, "r") as f:
#         reader = csv.DictReader(f)
#         return list(reader)


# def convert_to_json(data):
#     return json.dumps(data, indent=2)


if __name__ == "__main__":
    main()
