{
  description = "Archi Exploration and Processing";

  # Input dependancies
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
  # inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  ## Simply flake build
  inputs.flake-utils.url = "github:numtide/flake-utils";
  
  inputs.poetry2nix = {
    url = "github:nix-community/poetry2nix";
    # inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix}:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          local_pkgs = self.packages.${system};
          local_apps = self.apps.${system};
          inherit (poetry2nix.lib.mkPoetry2Nix { inherit pkgs; }) mkPoetryApplication;
          archi = mkPoetryApplication {
            projectDir = ./.;
          };
        in
        {
          packages.developshell = import ./shell.nix { inherit pkgs; };
          # Build and run a poetry deployment shell
          packages.archi = archi;
          # Add the package to the packages and the develop set
          packages.default = local_pkgs.developshell;
          devShells.default = local_pkgs.developshell;

          # run following archi will be available
          # nix develop .#archishell

          packages.archishell = pkgs.mkShell rec {
            name = "archi";
            buildInputs = with pkgs; [
              (python310.withPackages (ps: with ps; [poetry]))
              archi
            ];
            shellHook = ''
              echo "hello"
            '';
          };
          # packages.manage = pkgs.runCommand "manage" { buildInputs = [ ]; }
          #   ''
          #     echo "manage"
          #   '';
          # apps.manage = flake-utils.lib.mkApp { drv = local_pkgs.manage; };
        }
      );
}
