import click
from archi.converters import read_csv, add_links_to_nodes, convert_to_json


@click.command()
@click.argument("nodes_file", type=click.Path(exists=True))
@click.argument("links_file", type=click.Path(exists=True))
@click.argument("output_file", type=click.Path())
def main(nodes_file, links_file, output_file):
    nodes = read_csv(nodes_file)
    links = read_csv(links_file)
    add_links_to_nodes(nodes, links)
    json_data = convert_to_json(nodes, links)
    with open(output_file, "w") as f:
        f.write(json_data)
    click.echo(f"Successfully converted {nodes_file} and {links_file} to {output_file}")


if __name__ == "__main__":
    main()
