"""
URL configuration for archihub project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include, re_path
from archiapp import views

from django.conf.urls.static import static

urlpatterns = [
    *static(settings.STATIC_URL, document_root=settings.STATIC_ROOT),
    path("", include("social_django.urls", namespace="social")),
    path("admin/", admin.site.urls),
    path("", views.index, name="index"),
    path("upload.html", views.upload_file, name="upload"),
    re_path(r"reports/(?P<path>.*)", views.serve_report, name="report"),
]
