import json
import yaml
from archi.pluginmanager import PluginManager
from jinja2 import Environment, FileSystemLoader, select_autoescape
from markdown import markdown

def datetime_format(value, format="%H:%M %d-%m-%y"):
    return value.strftime(format)

def to_json(value):
    return json.dumps(value)
    # return json.dumps(remove_circular_refs(value))

def to_yaml(value):
    return yaml.dump(value)

def from_markdown(value):
    return markdown(value)

def find_by_attr(key, l, key_attribute="id"):
    return [ele for ele in l if ele[key_attribute] == key]

# Useful circular ref handeler
# Taken from https://stackoverflow.com/questions/44777369/remove-circular-references-in-dicts-lists-tuples
def remove_circular_refs(ob, _seen=None):
    if _seen is None:
        _seen = set()
    if id(ob) in _seen:
        # circular reference, remove it.
        return None
    _seen.add(id(ob))
    res = ob
    if isinstance(ob, dict):
        res = {
            remove_circular_refs(k, _seen): remove_circular_refs(v, _seen)
            for k, v in ob.items()}
    elif isinstance(ob, (list, tuple, set, frozenset)):
        res = type(ob)(remove_circular_refs(v, _seen) for v in ob)
    # remove id again; only *nested* references count
    _seen.remove(id(ob))
    return res

def build_template_environment():
    environment = Environment(
        loader=FileSystemLoader(["./", "/"]),
        autoescape=select_autoescape()
    )
    return environment

def add_basic_filters_to_environment(environment):
    environment.filters["datetime_format"] = datetime_format
    environment.filters["to_json"] = to_json
    environment.filters["to_yaml"] = to_yaml
    environment.filters["from_markdown"] = from_markdown
    environment.filters["find_by_attr"] = find_by_attr
    # environment.filters["simple_filter"] = find_by_attr

def template(template_file, **kwargs):
    environment = build_template_environment()
    add_basic_filters_to_environment(environment)
    template = environment.get_template(template_file)
    return template.render(**kwargs)

def template_plugins(template_file, plugin_path, **kwargs):
    environment = build_template_environment()
    add_basic_filters_to_environment(environment)
# 
    # print(f"Plugin path: {plugin_path}")
    plugins = PluginManager.load_plugins(plugin_path)
    print(f"Plugin path: {plugins}")
    PluginManager.run_plugins(plugins, environment)
    template = environment.get_template(template_file)
    return template.render(**kwargs)