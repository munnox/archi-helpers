# Derived from https://mathieularose.com/plugin-architecture-in-python July 2020
# Original Ideas from https://github.com/larose/utt under GLPv3

# TODO: Move the Plugin manager to hold plugins and context
# TODO: allow multiple plugin paths to be used
# TODO: Decide to allow to allow mutations or not.... Currently thinking to limit to just the filter system...

from abc import ABC, abstractmethod

import importlib
import pkgutil
import sys


class PluginBase(ABC):
    @abstractmethod
    def run(self, *args, **kwargs):
        pass

class PluginManager:

    @staticmethod
    def load_plugins(plugin_path):
        sys.path.append(plugin_path)
        # for importer, modname, ispkg in pkgutil.walk_packages(path=[plugin_path]): #, prefix=package.__name__+'.', onerror=lambda x: None):
        #     print(f"walk_packages importer: {importer}, modname: {modname}, ispkg: {ispkg}")
        #     try:
        #         print(f"module name: {module}")
        #         modulesource = importlib.import_module(modname)
        #         # reload(modulesource)
        #         print("reloaded: {}".format(modname))
        #     except Exception as e:
        #         print('Could not load {} {}'.format(modname, e))
        print(f"path: {sys.path}")
        plugins = []
        for importer, modname, ispkg in pkgutil.iter_modules([plugin_path]):
            print(f"iter_modules importer: {importer}, modname: {modname}, ispkg: {ispkg}")
            # module = importlib.import_module(f"{plugin_path}.{modname}")
            # if (not ispkg):
            #     continue
            # module = importlib.import_module(f"plugins.{modname}")
            module = importlib.import_module(f"{modname}")
            print(f"module name: {module}")
            for attr in dir(module):
                cls = getattr(module, attr)
                if isinstance(cls, type) and issubclass(cls, PluginBase) and cls is not PluginBase:
                    plugins.append(cls())
        return plugins
    
    @staticmethod
    def run_plugins(plugins, *args, **kwargs):
        for plugin in plugins:
            plugin.run(*args, **kwargs)