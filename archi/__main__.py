# import json
import os
from typing import Any, Dict, Type, List
from archi.pluginmanager import PluginManager
from archi.templating import template_plugins
import click
import traceback, sys
import logging

from archi.models import Archimodel

from .converters import (
    convert_separate_tables_nodes,
    convert_separate_tables_links
    # convert_model_element_to_special_groups,
    # convert_elements_specialisation_groups_to_nodeprops,
    # convert_to_csv,
)
from .opensearchhandler import OpensearchHandler

import archi.storage as storage


# logger = logging.getLogger("main").setLevel(logging.INFO)
# logging.getLogger("matplotlib").setLevel(logging.INFO)
logger = logging.getLogger(__name__)

def setup_logging(verbose):
    level = logging.ERROR
    if verbose == 1:
        level = logging.WARNING
    if verbose == 2:
        level = logging.INFO
    if verbose == 3:
        level = logging.DEBUG
    logging.basicConfig(
        format="%(asctime)s|%(levelname)s|%(name)s|%(funcName)s:%(lineno)d|>%(message)s",
        level=level,
        datefmt="%H:%M:%S",
        stream=sys.stderr,
    )
    # Can Specify a Opensearch URI Enviroment variable as https://admin:admin@localhost:9200
    opensearch_uri = os.getenv("OPENSEARCH_URI", None)
    opensearch_logging_index = os.getenv("OPENSEARCH_LOGGING_INDEX", "logger_archi")
    if opensearch_uri is not None:
        if (verbose > 0):
            click.echo("Using Opensearch Logging")
        ophand = OpensearchHandler(
            opensearch_uri, opensearch_logging_index, index_clear=True
        )
        ophand.setLevel(logging.INFO)
        logging.getLogger().addHandler(ophand)
        logging.getLogger("opensearch").setLevel(logging.ERROR)
    else:
        if (verbose > 0):
            click.echo("Not Using Opensearch Logging")

@click.command()
@click.argument("model_file", type=click.Path(exists=True))
@click.argument("output_file", type=click.Path())
@click.option('-f', '--format-type', default="yaml")
@click.option('-v', '--verbose', count=True, help="Increase verbosity level.")
# @click.argument("output_file", type=click.Path())
def translate_archimate(model_file, output_file, format_type, verbose):  # , output_file):

    setup_logging(verbose)

    if (verbose > 0):
        click.echo("Running Archi Library Translate.")

    try:
        try:
            archi_model = Archimodel.from_archifile(model_file)
        except Exception as error:
            click.echo(f"File load error -> {error}")
            return

        dict_model = archi_model.to_dict()

        if (verbose > 0):
            click.echo(f"Archi model file read")
        # dump_json(json_file, dict_model)

        if format_type == 'yaml':
            storage.dump_yaml(output_file, dict_model)
            # storage.dump_yaml("temp/archi.yaml", archi_model.original_dict)
            # storage.write_xml_file("temp/archi.xml", archi_model.original_dict)
        if format_type == 'csv':
            csv_dict_model_nodes = convert_separate_tables_nodes(archi_model)
            csv_dict_model_links = convert_separate_tables_links(archi_model)

            logger.info(f"Write output files: /tmp/elements_types.csv /tmp/relations.csv")

            special_group_nodes = csv_dict_model_nodes["specialisation_groups"]
            # export_files = []
            for specialisation_key in special_group_nodes:
                # if type(specialisation_key) != type(tuple()):
                #     specialisation_key = tuple([specialisation_key])
                file_safe_specialisation = [
                    ele.replace("/", "_").replace(":", "_") for ele in specialisation_key
                ]
                file_name = f"nodes_{'-'.join(file_safe_specialisation)}.csv"
                convert_to_csv(
                    os.path.join("/tmp/", file_name),
                    special_group_nodes[specialisation_key]["aligned_elements"],
                    [
                        "ID",
                        "Name",
                        "Type",
                        "Specialization",
                        "Documentation",
                        "Class_Name",
                        "Class_Columns",
                    ],
                )

            special_group_links = csv_dict_model_links["specialisation_groups"]
            # export_files = []
            for specialisation_key in special_group_links:
                # if type(specialisation_key) != type(tuple()):
                #     specialisation_key = tuple([specialisation_key])
                file_safe_specialisation = [
                    ele.replace("/", "_").replace(":", "_") for ele in specialisation_key
                ]
                file_name = f"links_{'-'.join(file_safe_specialisation)}.csv"
                convert_to_csv(
                    os.path.join("/tmp/", file_name),
                    special_group_links[specialisation_key]["aligned_elements"],
                    [
                        "ID",
                        "Name",
                        "Type",
                        "Specialization",
                        "Documentation",
                        "Source",
                        "Target",
                        "Class_Name",
                        "Class_Columns",
                    ],
                )
            

    except Exception as error:
        click.echo(f"Error type({type(error)}) Message: {error}")
        click.echo(f"format_exc: {traceback.format_exc()}")
    if (verbose > 0):
        click.echo("Translate Complete.")


@click.command()
@click.argument("model_file", type=click.Path(exists=True))
@click.argument("template_file", type=click.Path(exists=True))
@click.argument("output_file", type=click.Path())
@click.option('--plugin-path', default='archi.plugins', help='Path to the plugins directory.')
@click.option('-f', '--format-type', default="yaml")
@click.option('-v', '--verbose', count=True, help="Increase verbosity level.")
# @click.argument("output_file", type=click.Path())
def template_archimate(model_file, template_file, output_file, plugin_path, format_type, verbose):  # , output_file):

    setup_logging(verbose)

    if (verbose > 0):
        click.echo("Running Archi Library Templating.")

    try:
        try:
            archi_model = Archimodel.from_archifile(model_file)
        except Exception as error:
            click.echo(f"File load error -> {error}")
            return

        dict_model = archi_model.to_dict()

        if (verbose > 0):
            click.echo(f"Archi model file read")
        # dump_json(json_file, dict_model)

        # Process relationships into the dict to make traversals easier
        nodes_dict = {n['ID']: n for n in dict_model['nodes']}
        for node in dict_model['nodes']:
            node['outrels'] = [ { "rel": r, "node": nodes_dict.get(r['Target'], None) } for r in dict_model['links'] if r['Source'] == node['ID']]
            node['inrels'] = [ { "rel": r, "node": nodes_dict.get(r['Source'], None) } for r in dict_model['links'] if r['Target'] == node['ID']]

        if format_type == 'yaml':
            from .templating import template


            # d = template(template_file, model=archi_model, model_dict=dict_model) 
            d = template_plugins(template_file, plugin_path, model=archi_model, model_dict=dict_model) 
            with open(output_file, "w") as f:
                f.write(d)
            # storage.dump_yaml(output_file, dict_model)
            # storage.dump_yaml("temp/archi.yaml", archi_model.original_dict)
            # storage.write_xml_file("temp/archi.xml", archi_model.original_dict)
       
            

    except Exception as error:
        click.echo(f"Error type({type(error)}) Message: {error}")
        click.echo(f"format_exc: {traceback.format_exc()}")

    if (verbose > 0):
        click.echo("Templating Complete.")


main = click.Group()
main.add_command(translate_archimate, "translate")
main.add_command(template_archimate, "template")

if __name__ == "__main__":
    main()
