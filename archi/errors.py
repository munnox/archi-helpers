import logging

logger = logging.getLogger(__name__)


class ArchiError(Exception):
    pass
