import logging
import dataclasses
from dataclasses import dataclass
from typing import Any, Dict, Optional, Type, List, Union, Hashable

from archi import storage
from archi.converters import (
    convert_elements_specialisation_groups_to_nodeprops,
    convert_model_element_to_special_groups,
)

from .errors import ArchiError

logger = logging.getLogger(__name__)

from abc import ABC, abstractmethod

# Abstract Class
class InterfaceToDict(ABC):
    @abstractmethod
    def to_dict(self):
        pass


@dataclass
class Element(InterfaceToDict):
    ID: str
    Name: str
    Documentation: str
    # Type: str
    Folder: list[str]
    Specialization: list[Any]
    Properties: list[Any]

    @classmethod
    def from_archi_dict(cls, meta, element, properties):
        """Convert a dict object to a Property object."""
        try:
            e = cls(
                **{
                    "ID": element["@id"],
                    "Name": element.get("@name", ""),
                    "Folder": meta.path,
                    "Documentation": element.get("documentation", ""),
                    "Properties": properties,
                    "Specialization": element.get("@profiles", ""),
                }
            )
        except KeyError as error:
            logger.error(f"Element Parse Error: {element}")
            raise ArchiError(
                f"Element to convert. Error: {error} Archi Element: {element}"
            ) from error
        return e

    def to_dict(self) -> Dict[str, Any]:
        """Convert a Element to a dict object."""
        try:
            return dataclasses.asdict(self)
        except Exception as error:
            raise ArchiError(
                f"'{type(self).__name__}' to convert. Error: {error} Element: {self}"
            ) from error


# @dataclass
# class Folder(Element):
#     pass


@dataclass
class TypedElement(Element):
    Type: str

    @classmethod
    def from_archi_dict(cls, meta, element, properties):
        """Convert a dict object to a Typed Element object."""
        try:
            elementtype = element["@xsi:type"].replace("archimate:", "")
        except KeyError:
            logger.debug(f"Typed element with no type: {element}")
            elementtype = ""
        try:
            # Allow the Base class to parse the base of the Element
            e = Element.from_archi_dict(meta, element, properties)
            # Convert the dataclass to the caller class and add the extra variable
            te = cls(Type=elementtype, **e.to_dict())
        except KeyError as error:
            logger.error(f"Typed Element Parse Error: {element}")
            raise ArchiError(
                f"Typed Element to convert. Error ({type(error)}): {error}\nArchi Element:\n{element}"
            ) from error
        return te

    # def to_dict(self) -> Dict[str, Any]:
    #     """Convert a Typed Element to a dict object."""
    #     try:
    #         return dataclasses.asdict(self)
    #     except Exception as error:
    #         raise ArchiError(
    #             f"Typed Element to convert. Error: {error} Element: {self}"
    #         ) from error


@dataclass
class Node(TypedElement):
    @staticmethod
    def is_archi_node(element):
        element_type = element.get("@xsi:type", None)
        result = "@source" not in element and "@conceptType" not in element
        logger.debug(f"Node found ({result}): {element_type}")
        return result


@dataclass
class Link(TypedElement):
    Source: str
    Target: str

    @staticmethod
    def is_archi_link(element):
        element_type = element.get("@xsi:type", None)
        result = "@source" in element and "@target" in element
        logger.debug(f"Link found ({result}): {element_type}")
        return result

    @classmethod
    def from_archi_dict(cls, meta, element, properties):
        """Convert a dict object to a Link object."""

        try:
            # Allow the Base class to parse the base of the Element
            te = TypedElement.from_archi_dict(meta, element, properties)
            # Convert the dataclass to the caller class and add the extra variable
            link = cls(
                Source=element["@source"], Target=element["@target"], **te.to_dict()
            )
        except KeyError as error:
            logger.error(f"Link Element Parse Error: {element}")
            raise ArchiError(
                f"Link Element to convert. Error ({type(error)}): {error}\nArchi Element:\n{element}"
            ) from error
        return link

    # def to_dict(self) -> Dict[str, Any]:
    #     """Convert a Link Element to a dict object."""
    #     try:
    #         return dataclasses.asdict(self)
    #     except Exception as error:
    #         raise ArchiError(
    #             f"Link Element to convert. Error: {error} Element: {self}"
    #         ) from error


@dataclass
class Folder(Element):
    SubElements: list[Any]

    @staticmethod
    def is_archi_folder(element):
        element_type = element.get("@xsi:type", None)
        result = "@type" in element
        logger.debug(f"Folder found ({result}): {element_type}")
        return result

    @classmethod
    def from_archi_dict(cls, meta, element, properties):
        """Convert a dict object to a Typed Element object."""
        # try:
        #     elementtype = element["@xsi:type"].replace("archimate:", "")
        # except KeyError:
        #     logger.debug(f"Typed element with no type: {element}")
        #     elementtype = ""
        try:
            # Allow the Base class to parse the base of the Element
            e = Element.from_archi_dict(meta, element, properties)
            # subfolders = process_subfolders(meta, element)

            elements = Folder.process_xml_element(meta, element)
            logger.debug(f"Elements: {elements}")
            subfolders = Folder.process_xml_folders(meta, element)
            logger.debug(
                f"process_folder: {meta} number of subfolders: {len(subfolders)}"
            )
            for folder in subfolders:
                logger.debug(f"Folder: {folder}")
                elements.update(folder.SubElements)
            # Convert the dataclass to the caller class and add the extra variable
            f = cls(SubElements=elements, **e.to_dict())
        except KeyError as error:
            logger.error(f"Typed Element Parse Error: {element}")
            raise ArchiError(
                f"Typed Element to convert. Error ({type(error)}): {error}\nArchi Element:\n{element}"
            ) from error
        return f

    @staticmethod
    def process_xml_element(meta, element):
        elements = {}
        if "element" in element:
            original_elements = element["element"]
            if type(original_elements) == type({}):
                logger.debug(f"Process_subelement: {original_elements.keys()}")
                original_elements = [original_elements]
            # meta = Meta()
            # meta.path = path
            elements = {
                e["@id"]: Archimodel.process_element(meta, e) for e in original_elements
            }
        return elements

    @staticmethod
    def process_xml_folders(meta, element):
        folders = []
        if "folder" in element:
            logger.debug(f"Folder found here: {element.keys()}")
            subfolders = element["folder"]
            if type(subfolders) == type({}):
                subfolders = [subfolders]
            # subfolder_names = [f.keys() for f in subfolders]
            logger.debug(f"Process_SubFolders: {element['@name']} {type(subfolders)}")
            logger.debug(f"Number of sub folders: {len(subfolders)}")
            subfolder_dict = {f["@name"]: f for f in subfolders}
            for subfolder in subfolder_dict.values():
                meta.path = meta.path + [subfolder["@name"]]
                # folder = process_folder(meta, subfolder)
                folder = Folder.from_archi_dict(meta, subfolder, properties={})
                logger.debug(f"process_subfolders: {folder}")
                folders.append(folder)
        else:
            logger.debug(f"No folders here: {element.keys()}")
        return folders


@dataclass
class Profile:
    ID: str
    Name: str
    ImagePath: Optional[str]
    ConceptType: str

    @staticmethod
    def is_archi_profile(element):
        element_type = element.get("@xsi:type", None)
        result = "@conceptType" in element
        logger.debug(f"Profile found ({result}): {element_type}")
        return result

    @classmethod
    def from_archi_dict(cls, meta, element, properties):
        """Convert a dict object to a Property object."""
        # TODO remove image path if not given...
        try:
            e = cls(
                **{
                    "ID": element["@id"],
                    "Name": element.get("@name", ""),
                    "ImagePath": element.get("@imagePath", ""),
                    "ConceptType": element["@conceptType"],
                }
            )
        except KeyError as error:
            logger.error(f"Element Parse Error: {element}")
            raise ArchiError(
                f"Element to convert. Error ({type(error)}): {error}\nArchi Element:\n{element}"
            ) from error
        return e

    def to_dict(self) -> Dict[str, Any]:
        """Convert a Element to a dict object."""
        try:
            return dataclasses.asdict(self)
        except Exception as error:
            raise ArchiError(
                f"'{type(self).__name__}' to convert. Error: {error} Element: {self}"
            ) from error


@dataclass
class Property:
    key: str
    value: Any

    @classmethod
    def from_archi_dict(cls, prop):
        """Convert a dict object to a Property object."""
        try:
            p = Property(prop["@key"], prop.get("@value", None))
        except KeyError as error:
            logger.error(f"Property Parse Error: {prop}")
            raise ArchiError(
                f"Property to convert. Error: {error} Archi Property: {prop}"
            ) from error
        return p

    def to_dict(self) -> Dict[str, Any]:
        """Convert a Property to a dict object."""
        try:
            return dataclasses.asdict(self)
        except Exception as error:
            raise ArchiError(
                f"Property to convert. Error: {error} Property: {self}"
            ) from error


@dataclass
class View(TypedElement):
    children: list[Any]

    @staticmethod
    def is_archi_view(element):
        element_type = element.get("@xsi:type", None)
        result = "archimate:ArchimateDiagramModel" == element_type
        if result:
            logger.debug(f"View found ({result}): {element_type}")
        return result

    @classmethod
    def from_archi_dict(cls, meta, element, properties):
        """Convert a dict object to a Link object."""

        try:
            # Allow the Base class to parse the base of the Element
            te = TypedElement.from_archi_dict(meta, element, properties)
            logger.info(f"View Children {element}")
            children = View.process_xml_child(meta, element)
            # Convert the dataclass to the caller class and add the extra variable
            view = cls(children=children, **te.to_dict())
        except KeyError as error:
            logger.error(f"View Element Parse Error: {element}")
            raise ArchiError(
                f"View Element to convert. Error ({type(error)}): {error}\nArchi Element:\n{element}"
            ) from error
        return view

    @staticmethod
    def process_xml_child(meta, element):
        elements = {}
        if "child" in element:
            original_elements = element["child"]
            if type(original_elements) == type({}):
                logger.debug(f"Process_child: {original_elements.keys()}")
                original_elements = [original_elements]
            # meta = Meta()
            # meta.path = path
            elements = {
                e["@id"]: Archimodel.process_element(meta, e) for e in original_elements
            }
        return elements


@dataclass
class DiagramObject(TypedElement):
    x: int
    y: int
    width: int
    height: int
    label: str
    modelid: str
    content: str
    connections: Any

    @staticmethod
    def is_archi_diagramobject(element):
        element_type = element.get("@xsi:type", None)
        result = (
            ("archimate:DiagramObject" == element_type)
            or ("archimate:Note" == element_type)
            or ("archimate:Group" == element_type)
        )
        if result:
            logger.debug(f"Diagram Object found ({result}): {element_type}")
        return result

    @classmethod
    def from_archi_dict(cls, meta, element, properties):
        """Convert a dict object to a Display Object."""

        try:
            # Allow the Base class to parse the base of the Element
            te = TypedElement.from_archi_dict(meta, element, properties)
            logger.info(f"View Diagram Object {element}")

            # bounds = {}
            # raw_bounds = {}
            # if 'bounds' in element:
            raw_bounds = element.get("bounds", {})
            bounds = {
                "x": raw_bounds.get("@x", 0),
                "y": raw_bounds.get("@y", 0),
                "width": raw_bounds.get("@width", 0),
                "height": raw_bounds.get("@height", 0),
            }
            feature = {"label": None, "content": None, "connections": None}
            if "feature" in element:
                if element["feature"]["@name"] == "labelExpression":
                    feature["label"] = element["feature"]["@value"]
            if "content" in element:
                feature["content"] = element["content"]

            if "sourceConnection" in element:
                # Pull the list of connection type with "archimate:Connection"
                feature["connections"] = element["sourceConnection"]

            # Convert the dataclass to the caller class and add the extra variable
            obj = cls(
                modelid=element.get("@archimateElement", None),
                **bounds,
                **feature,
                **te.to_dict(),
            )
        except KeyError as error:
            logger.error(f"Diagram Object Element Parse Error: {element}")
            raise ArchiError(
                f"Diagram Object Element to convert. Error ({type(error)}): {error}\nArchi Element:\n{element}"
            ) from error
        return obj


class Meta:
    path: list[str]


class Archimodel:
    modelelement: Element
    elements: list[Node]
    links: list[Link]
    views: list[Any]
    folders: dict[str, Folder]
    profiles: dict[str, Profile]

    original_dict: dict[str, Any]

    def __init__(self):
        self.modelelement = None
        self.elements = []
        self.links = []
        self.views = []
        self.folders = {}
        self.profiles = {}

        self.original_dict = {}

    def to_dict(self):
        json_model = {
            "nodes": [self.modelelement.to_dict()],
            "links": Archimodel.convert_collection_to_dict(self.links),
            "views": Archimodel.convert_collection_to_dict(self.views),
            "folder_layout": self.folders,
            "profiles": Archimodel.convert_collection_to_dict(self.profiles),
        }
        json_model["nodes"].extend(Archimodel.convert_collection_to_dict(self.elements))

        return json_model

    @staticmethod
    def from_archifile(input_archi):
        try:
            archi_dict_model = storage.read_archi_file(input_archi)
        except Exception as error:
            error_msg = f"File load error -> {error}"
            logger.error(error_msg)
            raise ArchiError(error_msg) from error
            # return

        # logger.info(f"Model keys: {archi_dict_model.keys()} {archi_dict_model['archimate:model'].keys()} {archi_dict_model['archimate:model']['profile']}")
        archi_model = Archimodel.from_archimodel(archi_dict_model)
        archi_model.original_dict = archi_dict_model
        return archi_model

    @staticmethod
    def from_archimodel(model):
        # logger.info("top of file")
        try:
            main_model = model["archimate:model"]
        except KeyError as error:
            logger.error(f"No archimate:model stucture to process.")
        folders = {f["@name"]: f for f in main_model["folder"]}
        logger.info(f"Main folders found: {folders.keys()}")
        # TODO Need to extracts profiles for specialisations
        archimodel = Archimodel()
        meta = Meta()
        meta.path = []

        archimodel.modelelement = Node(
            **{
                "ID": main_model["@id"],
                "Type": "ArchimateModel",
                "Specialization": "",
                "Name": main_model["@name"],
                "Documentation": main_model.get("purpose", None),
                "Folder": [],
                "Properties": [],
            }
        )

        # Check if there is only one object its only uses a dict not a list
        profiles = main_model.get("profile", [])
        if type(profiles) == type({}):
            profiles = [profiles]
        try:
            archimodel.profiles = {
                e["@id"]: Archimodel.process_element(meta, e) for e in profiles
            }
        except Exception as error:
            error_msg = f"Error: {error}. Profile infomation in model: {main_model.get('profile', 'Profile Not found')}"
            logger.error(error_msg)
            raise ArchiError(error_msg) from error
        # Process all folders placing the elements under the different areas:
        # links - relations
        # views - Views
        # elements - all others
        for name in folders:
            folder = folders[name]
            meta.path = [name]
            # Process the folder element as they are top level
            processed_folder = Archimodel.process_element(meta, folder)
            # Pull the subelement out of the folder
            subelements = processed_folder.SubElements

            # If the folder is relations then push
            # the subelements into links
            if "Relations" == name:
                archimodel.links.extend(subelements.values())
            # If the folder is view push the viewable objects into view
            elif "Views" == name:
                archimodel.views.extend(subelements.values())
            # Otherwise add them to the elements of the model
            else:
                archimodel.elements.extend(subelements.values())

            subelements_info = [{"name": se.Name} for se in subelements.values()]
            logger.info(
                f"Main Level Folder name: '{name}' number of element: {len(subelements)}"
            )  # , Type: {types}")
            # logger.info(f"Main Level Folder Elements: {subelements.keys()}")
            def get_type(e):
                # NOTE: Folder Type do not have a Type field
                # The issues is should they right now I don't not think so
                # they are not a normal element with a type
                try:
                    return {"type": e.Type}
                except:
                    return {"type": e.__class__.__name__}

            types = [get_type(e) for e in subelements.values()]
            processed_folder.SubElements = None
            archimodel.folders[name] = {
                "folder": processed_folder.to_dict(),
                "element_types": types,
            }
        return archimodel

    # @staticmethod
    # def convert_collection_to_dict(elements: List[InterfaceToDict]) -> List[Dict]:
    #     if type(elements) == type([]):
    #         return [e.to_dict() for e in elements]
    #     elif type(elements) == type({}):
    #         # logger.info(f"convert to dict: %s", elements)
    #         return [elements[k].to_dict() for k in elements]
    #     raise ArchiError("Should not get here. Fail noisy!!!")

    @staticmethod
    def process_xml_properties(element):
        properties = []
        if "property" in element:
            original_properties = element["property"]
            # Sketchs have a properties = None
            if original_properties is None:
                original_properties = []
            if type(original_properties) == type({}):
                original_properties = [original_properties]
            for prop in original_properties:
                # Use Property Class for type checking
                # Convert back to dict for ease of representation

                # the property will be None is there was blank one defined
                # NOTE: Current direction is to ignore the property
                if prop is not None:
                    properties.append(Property.from_archi_dict(prop).to_dict())
                else:
                    logger.warning(
                        f"Property entry is None element has a blank property, {element}"
                    )
        return properties

    @staticmethod
    def process_element(meta, element):
        # logger.info("Process element")

        try:
            properties = Archimodel.process_xml_properties(element)
        except Exception as error:
            error_msg = f"Property ({type(error)}) error: {error} - Element: {element}"
            logger.error(error_msg)
            raise ArchiError(error_msg) from error

        # Current test for a view element
        if View.is_archi_view(element):
            # It is a View
            view = View.from_archi_dict(meta, element, properties)  # .to_dict()
            return view
        # Current test for a DiagramObject element
        if DiagramObject.is_archi_diagramobject(element):
            # It is a diagramobject
            diagramobject = DiagramObject.from_archi_dict(
                meta, element, properties
            )  # .to_dict()
            return diagramobject
        # Current test for a folder element
        if Folder.is_archi_folder(element):
            # It is a folder
            folder = Folder.from_archi_dict(meta, element, properties)  # .to_dict()
            return folder
        # Current test for a node element
        elif Node.is_archi_node(element):
            node = Node.from_archi_dict(meta, element, properties)  # .to_dict()
            return node
        # Current Test for a profile element
        elif Profile.is_archi_profile(element):
            profile = Profile.from_archi_dict(meta, element, properties)  # .to_dict()
            return profile
        # Current test for a relation element
        elif Link.is_archi_link(element):
            link = Link.from_archi_dict(meta, element, properties)
            return link

        raise ArchiError(f"Should not get here. Fail Noisey: {element}")

    @staticmethod
    def convert_collection_to_dict(
        elements: Union[List[InterfaceToDict], Dict[Hashable, InterfaceToDict]]
    ) -> List[Dict[Hashable, Any]]:
        final_list = None
        try:
            if type(elements) == type([]):
                final_list = [e.to_dict() for e in elements]
            elif type(elements) == type({}):
                # logger.info(f"convert to dict: %s", elements)
                final_list = [elements[k].to_dict() for k in elements]
            else:
                raise ArchiError(f"Unknown elements type given: {type(elements)}")
        except Exception as error:
            raise ArchiError(f"Conversion error type({type(error)}) Message: {error}")
        return final_list

    def element_out_rels(self, node):
        nodes_dict = {n.ID: n for n in self.elements}
        def get_node(id):
            try:
                return nodes_dict[id]
            except KeyError:
                pass
        return [ { "rel": r, "node": get_node(r.Target) } for r in self.links if r.Source == node.ID]

    def element_in_rels(self, node):
        nodes_dict = {n.ID: n for n in self.elements}
        def get_node(id):
            try:
                return nodes_dict[id]
            except KeyError:
                pass
        return [ { "rel": r, "node": get_node(r.Source) } for r in self.links if r.Target == node.ID]

    def get_specialisation(self, element):
        # specialisations_dict = {n.ID: n for n in self.profiles}
        def get_specialisation(id):
            try:
                return self.profiles[id]
            except KeyError:
                pass
        return get_specialisation(element.Specialization)
        


# def process_folder(meta, element):
#     # elements = process_subelements(meta, element)
#     # subfolders = process_subfolders(meta, element)
#     # logger.debug(f"process_folder: {meta} number of subfolders: {len(subfolders)}")
#     # for folder in subfolders:
#     #     elements.update(folder.SubElements)

#     # folder_dict = {"subelements": elements, **folder}
#     # folder_dict = { **folder}
#     return Folder.from_archi_dict(meta, element, properties={})
