# coding: utf-8
"""
A simple program to make a inventory of all the file under a root directory given

The Opensearch Logging Handler

author: Robert Munnoch started 23/04/2023
"""
from __future__ import print_function
from __future__ import division
import datetime

import os
import logging
import json
from opensearchpy import OpenSearch

logger = logging.getLogger(__name__)


class OpensearchHandler(logging.Handler):
    def __init__(
        self,
        uri: str = "admin:admin@localhost:9200",
        index_name: str = "logger_index",
        extra: dict[str, any] = None,
        index_clear=False,
        handler_debug=False,
    ):
        super().__init__()
        self.uri = uri
        self.index_name = index_name
        self.extra = extra
        self.handler_debug = handler_debug

        if self.handler_debug:
            print(f"Opensearch Handler URI: {self.uri}")
        self.opensearch = OpenSearch(
            [self.uri],
            use_ssl=True,
            verify_certs=False,
            ssl_assert_hostname=False,
            ssl_show_warn=False,
        )
        index_body = {"settings": {"index": {"number_of_shards": 4}}}
        index_exists = self.opensearch.indices.exists(self.index_name)
        if self.handler_debug:
            print(
                f"{self.__class__.__name__} Index '{self.index_name}' exists?: {index_exists}"
            )
        if index_exists and index_clear:
            response = self.opensearch.indices.delete(self.index_name)
            if self.handler_debug:
                print(
                    f"{self.__class__.__name__} Index '{self.index_name}' deleted response: {response}"
                )
            index_exists = self.opensearch.indices.exists(self.index_name)
        if not index_exists:
            response = self.opensearch.indices.create(self.index_name, body=index_body)
            if self.handler_debug:
                print(
                    f"{self.__class__.__name__} Index '{self.index_name}' created response: {response}"
                )

    def emit(self, record):
        # import tzlocal
        # import pytz
        from zoneinfo import ZoneInfo

        now = datetime.datetime.now(tz=ZoneInfo("Europe/London"))
        record_created = datetime.datetime.fromtimestamp(
            record.created, tz=ZoneInfo("Europe/London")
        )
        document = {
            # 'record': repr(record),
            "__class__.__name__": self.__class__.__name__,
            "name": record.name,
            "timestamp": now.isoformat(),
            "created": record_created.isoformat(),
            "relativeCreated": record.relativeCreated,
            "message": record.message,
            "formatted_message": self.format(record),
            "levelname": record.levelname,
            "levelno": record.levelno,
            "args": record.args,
            "module": record.module,
            "filename": record.filename,
            "funcName": record.funcName,
            "lineno": record.lineno,
            "pathname": record.pathname,
            "process": record.process,
            "processName": record.processName,
            "thread": record.thread,
            "threadName": record.threadName,
            "stack_info": record.stack_info,
            "exc_info": record.exc_info,
            "exc_text": record.exc_text,
        }
        if self.extra is not None:
            document.update(self.extra)
        id = now.isoformat().replace(":", "").lower()
        if self.handler_debug:
            print(f"Document:\n{document}")

        try:
            # self.opensearch.index
            response = self.opensearch.index(
                index=self.index_name, body=document, id=id, refresh=True
            )
        except Exception as error:
            pass


def setup_opensearch_handler():
    if opensearch_uri is None:
        print("Not Using Openstack Logging")
    else:
        print("Using Openstack Logging")
        # Can Specify a Opensearch URI Enviroment variable as https://admin:admin@localhost:9200
        opensearch_uri = os.getenv("OPENSEARCH_LOGGING_URI", None)
        opensearch_logging_index = os.getenv("OPENSEARCH_LOGGING_INDEX", "logger_index")
        ophand = OpensearchHandler(
            opensearch_uri,
            opensearch_logging_index,
            index_clear=True
            # extra={
            #     'subdict': {
            #         "test1": "val1",
            #         "test2": ["val2a", "val2b"],
            #     },
            #     # the following does not translate in to the opensearch document it is saved but not indexed.
            #     'sublist': [
            #         {
            #             "test1": "val1",
            #         },
            #         {
            #             "test2": ["val2a", "val2b"],
            #         }
            #     ]
            # },
        )
        ophand.setLevel(logging.INFO)
        logging.getLogger().addHandler(ophand)
        logging.getLogger("opensearch").setLevel(logging.ERROR)
