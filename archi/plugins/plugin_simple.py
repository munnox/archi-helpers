from archi.pluginmanager import PluginBase


def simple_filter(value):
    return "Hello from SimplePlugin"

class SimplePlugin(PluginBase):
    def run(self, *args, **kwargs):
        environment = args[0]
        print(f"Hello from HelloPlugin! Given: {args} -> {environment} going to add a simple filter...")
        environment.filters["simple_filter"] = simple_filter