import csv
import json
import logging
from typing import Any, Callable, Dict, List
import uuid

# from .models import InterfaceToDict, Archimodel, Meta, Node, process_element
from .errors import ArchiError

logger = logging.getLogger(__name__)


def convert_model_element_to_special_groups(
    elements, profiles, hashing_function: Callable[[Any, Any], tuple] = None
):
    # Convert an Archi model class to a nodeprop and relations

    # Collect Nodes and survey specialisations
    specialisations = {}
    for ele in elements:
        specialisation_profile_id = ele.Specialization

        try:
            specialisation_profile = profiles[specialisation_profile_id]
            assert specialisation_profile.ConceptType == ele.Type
            if hashing_function is None:
                specialisation_profile_type = (
                    specialisation_profile.ConceptType,
                    specialisation_profile.Name,
                )
            else:
                specialisation_profile_type = hashing_function(
                    ele, specialisation_profile
                )
        except KeyError as error:
            # NOTE: Note there is a comma after this must be a tuple even with one element
            # This value is used as a type hash as so must be hashable
            if hashing_function is None:
                specialisation_profile_type = (ele.Type,)
            else:
                specialisation_profile_type = hashing_function(ele, None)

        # Test and initialise the specialisation
        if specialisation_profile_type not in specialisations:
            specialisations[specialisation_profile_type] = {
                "elements": [],
                "columns": [],
                "aligned_elements": [],
            }

        # Add property name to the specialisation columns
        for prop in ele.Properties:
            # node_dict[prop['key']] = prop['value']
            if (
                prop["key"]
                not in specialisations[specialisation_profile_type]["columns"]
            ):
                specialisations[specialisation_profile_type]["columns"].append(
                    prop["key"]
                )

        specialisations[specialisation_profile_type]["elements"].append(ele)

    return specialisations

def convert_separate_tables_nodes(
    archimodel,
    class_type_separator="|",
    column_separator="|",
    property_separator="|",
    default_property_value="",
):
    special_groups_nodes = convert_model_element_to_special_groups(
        archimodel.elements, archimodel.profiles
    )

    def make_node(
        special_type,
        group,
        ele,
        special_name,
        class_type_separator,
        column_separator,
    ):
        ele_dict = {
            "ID": ele.ID,
            "Name": ele.Name,
            "Type": ele.Type,
            "Specialization": special_name,
            "Class_Name": f"{class_type_separator.join(special_type)}",
            "Class_Columns": column_separator.join(group["columns"]),
            "Documentation": ele.Documentation,
        }
        return ele_dict

    csv_dict_model_nodes = convert_elements_specialisation_groups_to_nodeprops(
        archimodel,
        special_groups_nodes,
        make_node,
        class_type_separator=class_type_separator,
        column_separator=column_separator,
        property_separator=property_separator,
        default_property_value=default_property_value,
    )
    return csv_dict_model_nodes

def convert_separate_tables_links(
    archimodel,
    class_type_separator="|",
    column_separator="|",
    property_separator="|",
    default_property_value="",
):
    special_groups_links = convert_model_element_to_special_groups(
        archimodel.links, archimodel.profiles
    )

    def make_link(
        special_type,
        group,
        link,
        special_name,
        class_type_separator,
        column_separator,
    ):
        link_dict = {
            "ID": link.ID,
            "Name": link.Name,
            "Type": link.Type,
            "Specialization": special_name,
            "Documentation": link.Documentation,
            "Source": link.Source,
            "Target": link.Target,
            "Class_Name": f"{class_type_separator.join(special_type)}",
            "Class_Columns": column_separator.join(group["columns"]),
        }
        return link_dict

    csv_dict_model_links = convert_elements_specialisation_groups_to_nodeprops(
        archimodel,
        special_groups_links,
        make_link,
        class_type_separator=class_type_separator,
        column_separator=column_separator,
        property_separator=property_separator,
        default_property_value=default_property_value,
    )
    return csv_dict_model_links


def convert_elements_specialisation_groups_to_nodeprops(
    archimodel,
    specialisations,
    make_element,
    class_type_separator="|",
    column_separator="|",
    property_separator="|",
    default_property_value="",
):
    """Convert Specaialisation Groups to flatted nodes"""
    # Align nodes
    for special_type in specialisations:
        group = specialisations[special_type]
        columns = group["columns"]
        logger.info(
            f"Working on {special_type} there are {len(group['elements'])} Elements and {len(group['columns'])} Total Properties"
        )
        # breakpoint()
        for ele in group["elements"]:
            specialisation_profile = archimodel.profiles.get(ele.Specialization, None)
            if specialisation_profile is None:
                special_name = ""
            else:
                special_name = specialisation_profile.Name
            try:
                prop_dict = {}

                ele_dict = make_element(
                    special_type,
                    group,
                    ele,
                    special_name,
                    class_type_separator,
                    column_separator,
                )
                # ele_dict = {
                #     "ID": ele.ID,
                #     "Name": ele.Name,
                #     "Type": ele.Type,
                #     "Specialization": special_name,
                #     "Class_Name": f"{class_type_separator.join(special_type)}",
                #     "Class_Columns": column_separator.join(columns),
                #     "Documentation": ele.Documentation,
                # }

                # Collect properties into an array as they can be used multiple times
                for ele in ele.Properties:
                    key = ele["key"]
                    if key not in prop_dict:
                        prop_dict[key] = []
                    val = ele["value"]
                    if val is None:
                        val = default_property_value
                    prop_dict[key].append(val)

                # Run throught all the columns and find it
                # or set it to None
                for prop in columns:
                    if prop in prop_dict:
                        ele_dict[prop] = property_separator.join(prop_dict[prop])
                    else:
                        ele_dict[prop] = default_property_value
            except Exception as error:
                error_msg = f"Element Alignment ({type(error)}) Error: {error}\nElement info:\n{ele}\n{columns}\n{prop_dict}"
                logger.error(error_msg)
                raise ArchiError(error_msg) from error

            group["aligned_elements"].append(ele_dict)

    results = {
        "specialisation_groups": specialisations,
        # "links": convert_to_dict(archimodel.links),
    }
    return results


def convert_links_specialisation_groups_to_nodeprops(
    archimodel,
    specialisations,
    make_link,
    class_type_separator="|",
    column_separator="|",
    property_separator="|",
    default_property_value="",
):
    """Convert Specaialisation Groups to flatted nodes"""
    # Align nodes
    for special_type in specialisations:
        group = specialisations[special_type]
        columns = group["columns"]
        logger.info(
            f"Working on {special_type} there are {len(group['links'])} Links and {len(group['columns'])} Total Properties"
        )
        # breakpoint()
        for link in group["elements"]:
            specialisation_profile = archimodel.profiles.get(link.Specialization, None)
            if specialisation_profile is None:
                special_name = ""
            else:
                special_name = specialisation_profile.Name
            try:
                link_dict = make_link(
                    special_type,
                    group,
                    link,
                    special_name,
                    class_type_separator,
                    column_separator,
                )
                # link_dict = {
                #     "ID": link.ID,
                #     "Name": link.Name,
                #     "Type": link.Type,
                #     "Specialization": special_name,
                #     "Documentation": link.Documentation,
                #     "Source": link.Source,
                #     "Target": link.Target,
                #     "Class_Name": f"{class_type_separator.join(special_type)}",
                #     "Class_Columns": column_separator.join(columns),
                # }

                # Collect properties into an array as they can be used mulitple times
                prop_dict = {}
                for ele in link.Properties:
                    key = ele["key"]
                    if key not in prop_dict:
                        prop_dict[key] = []
                    val = ele["value"]
                    if val is None:
                        val = default_property_value
                    prop_dict[key].append(val)

                # Run throught all the columns and find it
                # or set it to None
                for prop in columns:
                    if prop in prop_dict:
                        link_dict[prop] = property_separator.join(prop_dict[prop])
                    else:
                        link_dict[prop] = default_property_value
            except:
                logger.info(f"Link info:\n{link}\n{columns}\n{prop_dict}")

            group["aligned_elements"].append(link_dict)

    results = {
        "specialisation_groups": specialisations,
        # "links": convert_to_dict(archimodel.links),
    }
    return results


def convert_csv_export_to_json(
    nodes_file, properties_file, links_file, output_file, embed_links=True
):
    nodes = read_csv(nodes_file)
    properties = read_csv(properties_file)
    links = read_csv(links_file)
    add_properties_to_nodes(nodes, links, properties)
    embed_links = False
    data = {"nodes": nodes}
    if embed_links:
        add_links_to_nodes(nodes, links)
    else:
        data.update({"links": links})
    json_data = convert_to_json(data)
    with open(output_file, "w") as f:
        f.write(json_data)


def convert_json_nodeprops(
    input_file,
    nodes_file,
    links_file,
    embed_links=False,
    property_separator="|",
    class_type_separator="|",
    column_separator="|",
    default_property_value=None,
):
    with open(input_file, "r") as f:
        data = json.load(f)
    nodes = data["nodes"]
    links = data["links"]
    embed_links = False
    if embed_links:
        links = extract_links_from_nodes(nodes)
    properties = extract_properties_from_nodes_and_links(nodes, property_separator)
    properties_keys = [p["Key"] for p in properties]
    properties_key_set = list(set(properties_keys))

    classes = build_nodes_classes(nodes, links, class_type_separator)
    add_property_key_to_nodes(
        nodes,
        classes,
        column_separator,
        class_type_separator,
        property_separator,
        default_property_value,
    )
    add_property_key_to_links(
        links,
        classes,
        column_separator,
        class_type_separator,
        property_separator,
        default_property_value,
    )
    remove_properties_and_links_from_nodes(nodes)
    remove_properties_and_links_from_links(links)

    first_keys = [
        "ID",
        "Name",
        "Type",
        "Specialization",
        "Class_Name",
        "Class_Columns",
        "Documentation",
    ]
    convert_to_csv(nodes_file, nodes, first_keys)
    first_keys = [
        "ID",
        "Name",
        "Type",
        "Specialization",
        "Class_Columns",
        "Class_Name",
        "Documentation",
        "Source",
        "Target",
    ]
    convert_to_csv(links_file, links, first_keys)
    return properties, properties_key_set, classes


def nodeprops_to_json(
    nodes_file,
    links_file,
    output_file,
    column_separator="|",
    class_type_separator="|",
    property_separator="|",
):
    nodes = read_csv(nodes_file)
    links = read_csv(links_file)
    nodes = fold_properties_onto_nodes(
        nodes, column_separator, class_type_separator, property_separator
    )
    # print(f"first node: {nodes[0].keys()}")
    links = fold_properties_onto_links(
        links, column_separator, class_type_separator, property_separator
    )
    add_links_to_nodes(nodes, links)
    # json_data = convert_to_json(nodes, links)
    json_data = convert_to_json({"nodes": nodes})
    with open(output_file, "w") as f:
        f.write(json_data)
    return json_data


def json_csv_import(
    input_file, nodes_file, links_file, properties_file, properties_separator="|"
):
    with open(input_file, "r") as f:
        data = json.load(f)
    nodes = data["nodes"]
    links = extract_links_from_nodes(nodes)
    properties = extract_properties_from_nodes_and_links(nodes, properties_separator)
    remove_properties_and_links_from_nodes(nodes)
    remove_properties_and_links_from_links(links)
    keys = ["ID", "Type", "Name", "Documentation", "Specialization"]
    convert_to_csv(nodes_file, nodes, keys)

    keys = ["ID", "Key", "Value"]
    convert_to_csv(properties_file, properties, keys)

    keys = ["ID", "Type", "Name", "Documentation", "Source", "Target", "Specialization"]
    if len(links) == 0:
        links = [dict(zip(keys, [None] * len(keys)))]
    convert_to_csv(links_file, links, keys)


# def remove_properties_and_links_from_nodes(nodes):
#     for node in nodes:
#         if "Properties" in node:
#             del node["Properties"]
#         if "Links" in node:
#             del node["Links"]


# def remove_properties_and_links_from_links(links):
#     for link in links:
#         if "Properties" in link:
#             del link["Properties"]
#         if "Links" in link:
#             del link["Links"]


# def extract_links_from_nodes(nodes):
#     links = []
#     for node in nodes:
#         if "Links" in node:
#             links.extend(node["Links"])
#     links = {l["ID"]: l for l in links}
#     links = list(links.values())
#     return links


# def extract_properties_from_nodes_and_links(nodes):
#     properties = []
#     for node in nodes:
#         if "Properties" in node:
#             properties.extend(node["Properties"])
#         if "Links" in node:
#             for link in node["Links"]:
#                 if "Properties" in link:
#                     properties.extend(link["Properties"])
#     properties = {p["ID"] + p["Key"]: p for p in properties}
#     properties = list(properties.values())
#     return properties


def add_properties_to_nodes(nodes, links, properties):
    node_dict = {node["ID"]: node for node in nodes}
    link_dict = {link["ID"]: link for link in links}
    for prop in properties:
        if prop["ID"] in node_dict:
            node = node_dict[prop["ID"]]
            if "Properties" not in node:
                node["Properties"] = []
            node["Properties"].append(prop)
        if prop["ID"] in link_dict:
            link = link_dict[prop["ID"]]
            if "Properties" not in link:
                link["Properties"] = []
            link["Properties"].append(prop)


def add_links_to_nodes(nodes, links):
    node_dict = {node["ID"]: node for node in nodes}
    for link in links:
        source = node_dict.get(link["Source"], None)
        target = node_dict.get(link["Target"], None)
        # If the node can not be found drop the link
        # TODO: link are current dropped because of the intermediay json format
        # NOTE: this could be allowed to adjust the link and nodes separatly on import
        if source is not None and target is not None:
            if "Links" not in source:
                source["Links"] = []
            if "Links" not in target:
                target["Links"] = []
            source["Links"].append(link)
            target["Links"].append(link)


def fold_properties_onto_nodes(
    nodes, column_separator, class_type_separator, property_separator
):
    new_nodes = []
    for node in nodes:
        # If id is empty then create a uuid
        if node["ID"] == "":
            node["ID"] = "id-" + str(uuid.uuid4()).replace("-", "")
        class_parts = node["Class_Name"].split(class_type_separator)
        if len(class_parts) >= 1:
            # node_type = class_parts[0]
            if "Type" not in node:
                node["Type"] = class_parts[0]
        if len(class_parts) == 2:
            if "Specialization" not in node:
                node["Specialization"] = class_parts[1]

        property_columns = node["Class_Columns"].split(column_separator)
        properties = []
        if property_columns != [""]:
            # click.echo(f"Columns: {property_columns}")
            for column in property_columns:
                # click.echo(f"Node: {node} column: '{column}'")
                value = node[column]
                if property_separator in value:
                    parts = value.split(property_separator)
                    for part in parts:
                        properties.append(
                            {"ID": node["ID"], "Key": column, "Value": part}
                        )

                else:
                    properties.append({"ID": node["ID"], "Key": column, "Value": value})
        # click.echo(f"Properties: {properties}")
        keys_to_keep = ["ID", "Name", "Type", "Specialization", "Documentation"]
        keys_to_remove = set(node.keys()) ^ set(keys_to_keep)
        for key in keys_to_remove:
            del node[key]
        node["Properties"] = properties
        # click.echo(f"Node: {node.keys()}\nKeys to remove: {keys_to_remove}")
        new_nodes.append(node)
    return new_nodes


def fold_properties_onto_links(
    links, column_separator, class_type_separator, property_separator
):
    new_links = []
    for link in links:
        class_parts = link["Class_Name"].split(class_type_separator)
        if len(class_parts) >= 1:
            # node_type = class_parts[0]
            if "Type" not in link:
                link["Type"] = class_parts[0]
        if len(class_parts) == 2:
            if "Specialization" not in link:
                link["Specialization"] = class_parts[1]

        property_columns = link["Class_Columns"].split(column_separator)
        properties = []
        if property_columns != [""]:
            for column in property_columns:

                value = link[column]
                if property_separator in value:
                    parts = value.split(property_separator)
                    for part in parts:
                        properties.append(
                            {"ID": link["ID"], "Key": column, "Value": part}
                        )

                else:
                    properties.append({"ID": link["ID"], "Key": column, "Value": value})

        keys_to_keep = [
            "ID",
            "Name",
            "Type",
            "Documentation",
            "Specialization",
            "Source",
            "Target",
        ]
        keys_to_remove = set(link.keys()) ^ set(keys_to_keep)
        for key in keys_to_remove:
            del link[key]
        # click.echo(f"Link: {link.keys()}\nKeys to remove: {keys_to_remove}")
        link["Properties"] = properties
        new_links.append(link)
    return new_links


def class_key_builder(node, class_type_separator):
    class_key = f"{node['Type']}"
    if node["Specialization"] != "":
        class_key += f"{class_type_separator}{node['Specialization']}"
    return class_key


def build_nodes_classes(nodes, links, class_type_separator):
    classes_node = {}
    classes_link = {}
    props_node = []
    props_link = []
    for node in nodes:
        class_key = class_key_builder(node, class_type_separator)
        if class_key not in classes_node:
            classes_node[class_key] = {
                "Element": "Node",
                "Type": node["Type"],
                "Specialization": node["Specialization"],
                "Property_Names": [],
            }

        if "Properties" in node:
            props = [p["Key"] for p in node["Properties"]]
            props_node.extend(props)
            classes_node[class_key]["Property_Names"] = list(
                set(classes_node[class_key]["Property_Names"] + props)
            )
    for link in links:
        class_key = class_key_builder(link, class_type_separator)
        if class_key not in classes_link:
            classes_link[class_key] = {
                "Element": "link",
                "Type": link["Type"],
                "Specialization": link["Specialization"],
                "Property_Names": [],
            }

        if "Properties" in link:
            props = [p["Key"] for p in link["Properties"]]
            props_link.extend(props)
            classes_link[class_key]["Property_Names"] = list(
                set(classes_link[class_key]["Property_Names"] + props)
            )
    classes = {
        "NodeProps": list(set(props_node)),
        "LinkProps": list(set(props_link)),
        "Node": classes_node,
        "Link": classes_link,
    }
    return classes


def add_property_key_to_nodes(
    nodes,
    classes,
    column_separator,
    class_type_separator,
    property_separator,
    default_property_value,
):
    for prop in classes["NodeProps"]:
        for node in nodes:
            node[prop] = ""
            class_key = class_key_builder(node, class_type_separator)
            node["Class_Columns"] = column_separator.join(
                classes["Node"][class_key]["Property_Names"]
            )
            node["Class_Name"] = class_key
            if "Properties" in node:
                # Collect properties into an array as they can be used mulitple times
                prop_dict = {}
                for ele in node["Properties"]:
                    key = ele["Key"]
                    if key not in prop_dict:
                        prop_dict[key] = []
                    if ele["ID"] in node["ID"]:
                        val = ele["Value"]
                        prop_dict[key].append(val)
                    # else:
                    #     val = default_property_value
                    # prop_dict[key].append(val)
                # Join properties and add to node
                for k in prop_dict:
                    prop_list = prop_dict[k]
                    # if p["ID"] in node["ID"]:
                    node[prop] = property_separator.join(prop_list)


def add_property_key_to_links(
    links,
    classes,
    column_separator,
    class_type_separator,
    property_separator,
    default_property_value,
):
    for prop in classes["LinkProps"]:
        for link in links:
            link[prop] = ""
            class_key = class_key_builder(link, class_type_separator)
            link["Class_Columns"] = column_separator.join(
                classes["Link"][class_key]["Property_Names"]
            )
            link["Class_Name"] = class_key
            # if "Properties" in link:
            #     for p in link["Properties"]:
            #         if prop == p["Key"]:
            #             if p["ID"] in link["ID"]:
            #                 link[prop] = p["Value"]
            if "Properties" in link:
                # Collect properties into an array as they can be used mulitple times
                prop_dict = {}
                for ele in link["Properties"]:
                    key = ele["Key"]
                    if key not in prop_dict:
                        prop_dict[key] = []
                    if ele["ID"] in link["ID"]:
                        val = ele["Value"]
                        prop_dict[key].append(val)
                    # else:
                    #     val = default_property_value
                    # prop_dict[key].append(val)
                # Join properties and add to link
                for k in prop_dict:
                    prop_list = prop_dict[k]
                    link[prop] = property_separator.join(prop_list)


def remove_properties_and_links_from_nodes(nodes):
    for node in nodes:
        if "Properties" in node:
            del node["Properties"]
        if "Links" in node:
            del node["Links"]
        # click.echo(f"Node: {node}")


def remove_properties_and_links_from_links(links):
    for link in links:
        if "Properties" in link:
            del link["Properties"]
        if "Links" in link:
            del link["Links"]


def extract_links_from_nodes(nodes):
    links = []
    for node in nodes:
        if "Links" in node:
            links.extend(node["Links"])
    links = {l["ID"]: l for l in links}
    links = list(links.values())
    return links


def extract_properties_from_nodes_and_links(nodes, property_separator):
    properties = []
    for node in nodes:
        if "Properties" in node:
            # expanded_properties = []
            for prop in node["Properties"]:
                if property_separator in prop["Value"]:
                    # property in a compound list
                    prop_parts = prop["Value"].split(property_separator)
                    for part in prop_parts:
                        properties.append(
                            {"ID": prop["ID"], "Key": prop["Key"], "Value": part}
                        )
                else:
                    properties.append(
                        {"ID": prop["ID"], "Key": prop["Key"], "Value": prop["Value"]}
                    )
            # properties.extend(expanded_properties)
        if "Links" in node:
            for link in node["Links"]:
                if "Properties" in link:
                    properties.extend(link["Properties"])
    # properties = {p["ID"] + "-" + p["Key"]: p for p in properties}
    # properties = list(properties.values())
    return properties


# def convert_to_csv_1(filepath, data, keys):
#     # keys = data[0].keys()
#     with open(filepath, "w", newline="") as f:
#         dict_writer = csv.DictWriter(f, keys)
#         dict_writer.writeheader()
#         dict_writer.writerows(data)


def convert_to_csv(filepath, data, first_keys=None):
    # TODO: Break on edge case whewre is NO items
    if first_keys is None:
        first_keys = []
    all_keys = list(data[0].keys())
    rest_keys = list(set(first_keys) ^ set(all_keys))
    # click.echo(f"All keys: {all_keys}")
    # click.echo(f"first_keys: {first_keys}")
    # click.echo(f"rest_keys: {rest_keys}")
    # for key in all_keys:
    #     if key not in first_keys:
    #         rest_keys += [key]
    with open(filepath, "w", newline="") as f:
        dict_writer = csv.DictWriter(f, first_keys + list(rest_keys))
        dict_writer.writeheader()
        dict_writer.writerows(data)


def add_links_to_nodes_v2(nodes, links):
    node_dict = {node["id"]: node for node in nodes}
    for link in links:
        source = node_dict[link["source"]]
        target = node_dict[link["target"]]
        if "links" not in source:
            source["links"] = []
        if "links" not in target:
            target["links"] = []
        source["links"].append(link)
        target["links"].append(link)


def read_csv_v2(filepath):
    with open(filepath, "r") as f:
        reader = csv.DictReader(f)
        return list(reader)


def convert_to_json_v2(nodes, links):
    data = {"nodes": nodes, "links": links}
    return json.dumps(data)


def read_csv(filepath):
    with open(filepath, "r") as f:
        reader = csv.DictReader(f)
        return list(reader)


def read_json(filepath):
    with open(filepath, "r") as f:
        return json.load(f)


def convert_to_json(data):
    return json.dumps(data, indent=2)
