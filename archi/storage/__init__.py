import logging
from zipfile import ZipFile
import xmltodict
import yaml

logger = logging.getLogger(__name__)


def read_archi_file(filename: str) -> dict[str, str]:
    """Read a named xml file and return a dict of the xml."""
    result = None
    if filename.endswith(".xml"):
        with open(filename, encoding="utf-8") as fh:
            result = xmltodict.parse(fh.read())
    elif filename.endswith(".archimate"):
        # Try to read basic Archi XML
        # If that failes try to read it as a zip file
        # Archi uses a raw xml when no images have been
        # added then switch to a ZIP archive when images are used.
        try:
            with open(filename, encoding="utf-8") as fh:
                result = xmltodict.parse(fh.read())
        except:
            # extract the xml file out of the archimate zip archive
            with ZipFile(filename) as zf:
                if "model.xml" in zf.namelist():
                    result = xmltodict.parse(zf.read("model.xml"))
                else:
                    raise Exception(f"NO Model.xml file in archive {filename}.")
                # for name in zf.namelist()}
    if result is None:
        raise Exception(f"NO Models in file in {filename}.")
    return result


def write_xml_file(filename, d: dict[str, str]):
    """Write a dict of a xml to a named file."""
    with open(filename, "w", encoding="utf-8") as fh:
        fh.write(xmltodict.unparse(d, pretty=True))


# def dump_json(json_filename, d: dict[str, str]) -> str:
#     json_data = json.dumps(d, indent=2)
#     with open(json_filename, "w") as f:
#         f.write(json_data)


def dump_yaml(yaml_filename, d: dict[str, str]) -> str:
    # suppress the use of anchors
    class NoAliasIndentDumper(yaml.SafeDumper):
        def ignore_aliases(self, data):
            return True

        def increase_indent(self, flow=False, indentless=False):
            return super(NoAliasIndentDumper, self).increase_indent(flow, False)

    # yaml_data = yaml.dump(d, indent=10, Dumper=NoAliasDumper)
    with open(yaml_filename, "w") as f:
        yaml.dump(d, stream=f, allow_unicode=True, Dumper=NoAliasIndentDumper)
