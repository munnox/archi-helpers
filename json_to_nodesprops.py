import click
import csv
import json
import yaml
from archi.converters import convert_json_nodeprops


@click.command()
@click.argument("input_file", type=click.Path(exists=True))
@click.argument("nodes_file", type=click.Path())
@click.argument("links_file", type=click.Path())
def main(input_file, links_file, nodes_file):
    properties, properties_key_set, classes = convert_json_nodeprops(
        input_file, nodes_file, links_file
    )
    click.echo(f"Properties: {properties}")
    click.echo(f"Properties key: {properties_key_set}")
    click.echo(f"Classes:\n{yaml.dump(classes)}")
    click.echo(f"Successfully converted {input_file} to {nodes_file} and {links_file}")


if __name__ == "__main__":
    main()
