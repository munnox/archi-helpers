import click
import csv
import json
from archi.converters import json_csv_import


@click.command()
@click.argument("input_file", type=click.Path(exists=True))
@click.argument("nodes_file", type=click.Path())
@click.argument("properties_file", type=click.Path())
@click.argument("links_file", type=click.Path())
def main(input_file, nodes_file, properties_file, links_file):
    json_csv_import(input_file, nodes_file, links_file, properties_file)
    click.echo(
        f"Successfully converted {input_file} to {nodes_file}, {properties_file}, and {links_file}"
    )


if __name__ == "__main__":
    main()
