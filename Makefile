SHELL=/bin/bash
# CONTAINER = podman
CONTAINER_WEB_IMAGE = archi-helpers-web:0.1.0
CONTAINER_PROXY_IMAGE = archi-helpers-proxy:0.1.0
CONTAINER_GRAPH_IMAGE = archi-helpers-neo4j:0.1.0
CONTAINER_ARCHI_IMAGE = archi-helpers-archi:0.1.0
CONTAINER_ARCHI_PROCESSOR_IMAGE = archi-helpers-archi-processor:0.1.0


CONTAINER_WEB_NAME = archi-helpers
CONTAINER_PROXY_NAME = archi-helpers-proxy
CONTAINER_GRAPH_NAME = archi-helpers-graph
CONTAINER_ARCHI_NAME = archi-helpers-archi
CONTAINER_ARCHI_PROCESSOR_NAME = archi-helpers-archi-processor

LOCAL_PATH = $(shell pwd)
SRC_PATH = $(LOCAL_PATH)
DATA_PATH = $(LOCAL_PATH)/data
TEST_PATH = $(LOCAL_PATH)/test
TEMP_PATH = $(LOCAL_PATH)/temp

# Try to detect the container system and use one
# detected defaulting to docker if present
TESTPODMAN=$(shell which podman | sed -e 's/.*\(podman\)/\1/' - )
TESTDOCKER=$(shell which docker | sed -e 's/.*\(docker\)/\1/' - )
CONTAINER = docker
ifeq "$(TESTPODMAN)" "podman"
	CONTAINER = podman
endif
ifeq "$(TESTDOCKER)" "docker"
	CONTAINER = docker
endif

testbuilder:
	echo "Detected Builders are: '$(TESTPODMAN)' - '$(TESTDOCKER)'"

# build:
# 	$(CONTAINER) build -f containers/web/Dockerfile -t $(CONTAINER_WEB_IMAGE)

# From explore-django
build_archi:
	sudo $(CONTAINER) build \
		-f containers/archi/Dockerfile \
		-t $(CONTAINER_ARCHI_IMAGE) \
		${LOCAL_PATH}

build_archi_processor: build_archi
	sudo $(CONTAINER) build \
		-f containers/archi_processor/Dockerfile \
		-t $(CONTAINER_ARCHI_PROCESSOR_IMAGE) \
		${LOCAL_PATH}

build_web:
	sudo $(CONTAINER) build \
		--target=explore-unpriv \
		-f containers/web/Dockerfile \
		-t $(CONTAINER_WEB_IMAGE) \
		${LOCAL_PATH}

build_service:
	sudo $(CONTAINER) build \
		-f containers/proxy/Dockerfile \
		-t $(CONTAINER_PROXY_IMAGE) \
		${LOCAL_PATH}
	sudo $(CONTAINER) build \
		-f containers/graph/Dockerfile \
		-t $(CONTAINER_GRAPH_IMAGE) \
		${LOCAL_PATH}

build: build_archi build_web build_service

build_dev:
	sudo docker build -f containers/dev/Dockerfile -t archi-helper-dev:latest .

build_dev_nix:
	sudo docker build -f containers/dev_nix/Dockerfile -t archi-helper-dev:latest .

dev_app_vue:
	sudo docker run -it --rm \
		-v "$(shell pwd):/work" \
		-p 5173:5173 \
		-w /work/spa-vue/ \
		-e PATH="/work/spa-vue/node_modules/.bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin" \
		-e VITE_CONTAINER_VAR="RUNNING_IN_DEV" \
		node:latest \
			bash
# bash -c "export PATH=\"/work/spa-vue/node_modules/.bin:${PATH}\" && bash"


DEV_IMAGE=archi-helper-dev:latest
DEV_RUN_NAME=archi-help-dev-run-daemon

run:
	# --user $(shell id -u):$(shell id -g) \
	sudo docker run --rm -it \
		-v ${HOME}/.ssh:/root/.ssh:ro \
		-v ${shell pwd}/:/work/ \
		${DEV_IMAGE} \
		bash
#nix develop
# -v $(shell pwd):/home/base \

run_daemon:
	sudo docker run -d --name ${DEV_RUN_NAME} \
		-v ${HOME}/.ssh:/root/base.ssh:ro \
		${DEV_IMAGE}
# -v $(shell pwd):/home/base \

attach_daemon:
	sudo docker exec -it ${DEV_RUN_NAME} \
		cp -r /root/base.ssh/ /root/.ssh && nix develop

rm_daemon:
	sudo docker rm -f ${DEV_RUN_NAME}

shell_web: 
	@mkdir -p ${DATA_PATH}
	@mkdir -p ${TEST_PATH}
	@mkdir -p ${TEMP_PATH}
	sudo $(CONTAINER) run -it --rm \
		--name $(CONTAINER_WEB_NAME)-shell \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEST_PATH):/test/:z \
		-v $(TEMP_PATH):/tmp/:z \
		$(CONTAINER_WEB_IMAGE) \
			bash

shell_archi: 
	@mkdir -p ${DATA_PATH}
	@mkdir -p ${TEST_PATH}
	@mkdir -p ${TEMP_PATH}
	# -v $(DATA_PATH):/data/:z \
	sudo $(CONTAINER) run -it --rm \
		--name $(CONTAINER_ARCHI_NAME)-shell \
		--user $(shell id -u):$(shell id -g) \
		-v $(HOME)/.ssh/:/home/archi/.ssh/ \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEST_PATH):/test/:z \
		-v /certs:/certs/:z \
		$(CONTAINER_ARCHI_IMAGE) \
			bash
			
shell_archi_processor: 
	@mkdir -p ${DATA_PATH}
	@mkdir -p ${TEST_PATH}
	@mkdir -p ${TEMP_PATH}
	# -v $(DATA_PATH):/data/:z
	# --user $(shell id -u):$(shell id -g)
	# -v /certs:/certs/:z 
	sudo $(CONTAINER) run -it --rm \
		--name $(CONTAINER_ARCHI_PROCESSOR_NAME)-shell \
		-v $(HOME)/.ssh/:/home/archi/.ssh/ \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEST_PATH):/test/:z \
		-e ARCHI_REPO_URL=$(shell source .env; echo $$TEST_MODEL_REPO_URL) \
		-e ARCHI_HTTPS_PASSWORD=$(shell source .env; echo $$TEST_MODEL_REPO_TOKEN) \
		$(CONTAINER_ARCHI_PROCESSOR_IMAGE) \
			bash
			

# attach a bash shell to the local container
attach:
	$(CONTAINER) exec -it $(CONTAINER_WEB_NAME) bash

# Local run to detect model changes and create migrations
makemirgrations:
	sudo $(CONTAINER) run -it --rm \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:Z \
		$(CONTAINER_WEB_IMAGE) \
			poetry run web install_labels
	sudo $(CONTAINER) run -it --rm \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:Z \
		$(CONTAINER_WEB_IMAGE) \
			poetry run web makemigrations

# Local run to detect model changes and create migrations
clearneo4j:
	sudo $(CONTAINER) run -it --rm \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:Z \
		$(CONTAINER_WEB_IMAGE) \
			poetry run web clear_neo4j

# Run the migration -- To be remove as the migration is run on entry
migrate:
	$(CONTAINER) exec -it $(CONTAINER_WEB_NAME) poetry run web migrate

# Create a superuse for the container
createsuperuser:
	$(CONTAINER) exec -it $(CONTAINER_WEB_NAME) poetry run web createsuperuser --username admin --email a@b.com

webshell:
	$(CONTAINER) exec -it $(CONTAINER_WEB_NAME) poetry run web shell

# # Non persistant container to just run the server within the container
# deamon:
# 	$(CONTAINER) run -d --name $(CONTAINER_WEB_NAME) \
# 		--restart=always \
# 		-p $(EXPOSED_PORT):8000/tcp \
# 		$(CONTAINER_WEB_IMAGE)

local_stage_build:
	ANSIBLE_CONFIG=deployment/ansible/ansible.cfg poetry run ansible-playbook -i deployment/ansible/inventory.yaml deployment/ansible/build_push.yaml --extra-vars "state=present playbook_hosts=localhost"

local_stage_present:
	poetry run ansible-playbook -i deployment/ansible/inventory.yaml deployment/ansible/deploy.yaml --extra-vars "state=present playbook_hosts=localhost"

local_stage_absent:
	poetry run ansible-playbook -i deployment/ansible/inventory.yaml deployment/ansible/deploy.yaml --extra-vars "state=absent playbook_hosts=localhost"


# attached data folder and persistance and source folder to allow local changes to be reflected
develop: data
	$(CONTAINER) run -d --name $(CONTAINER_WEB_NAME) \
		--restart=always \
		-v $(SRC_PATH)/:/src/ \
		-e DEBUG=True \
		-e DJANGO_SUPERUSER_PASSWORD=your_password \
		-p 8000:8000/tcp \
		$(CONTAINER_WEB_IMAGE)
	$(CONTAINER) run -d --name $(CONTAINER_PROXY_NAME) \
		--restart=always \
		-p 8001:8001/tcp \
		$(CONTAINER_PROXY_IMAGE)
	$(CONTAINER) run -d --name $(CONTAINER_GRAPH_NAME) \
		--restart=always \
		--env NEO4J_AUTH=neo4j/your_password \
		-p 7474:7474/tcp \
		-p 7687:7687/tcp \
		$(CONTAINER_GRAPH_IMAGE)

# -e RUN_ENV=self \
# -e DATABASE_URL=sqlite:///data/db.sqlite3 \
# -e CERT_FILE=/certs/com.example.crt \
# -e KEY_FILE=/certs/com.example.key \
# -v $(DATA_PATH):/data/:Z \
# -v $(SRC_PATH)/certs:/certs/ \
# -v $(SRC_PATH)/:/src/ \
# -v $(DATA_PATH):/data/:Z \
# -v $(SRC_PATH)/:/src/ \
# -v $(DATA_PATH):/data/:Z \

# # peristance container with internal source
# deamon_persist: data
# 	$(CONTAINER) run -d --name $(CONTAINER_WEB_NAME) \
# 		--restart=always \
# 		-v $(DATA_PATH):/data/:Z \
# 		-p $(EXPOSED_PORT):8000/tcp \
# 		$(CONTAINER_WEB_IMAGE)

# Create the persistant data directory
# Set the correct permissions
data:
	@mkdir $@
	$(CONTAINER) unshare chown 200:200 -R $(DATA_PATH)

# View the logs of the container
logs:
	$(CONTAINER) logs -f $(CONTAINER_WEB_NAME)

# Remove the container 
rm:
	$(CONTAINER) rm -f $(CONTAINER_WEB_NAME)
	$(CONTAINER) rm -f $(CONTAINER_PROXY_NAME)
	$(CONTAINER) rm -f $(CONTAINER_GRAPH_NAME)

# Remove the remove data dir and container
clean: rm
	# $(CONTAINER) unshare rm -rf $(DATA_PATH)
	sudo rm -rf $(DATA_PATH)

convert_archi:
	$(CONTAINER) run -it --rm \
		--name $(CONTAINER_WEB_NAME)_shell \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEMP_PATH):/tmp/:z \
		$(CONTAINER_WEB_IMAGE) \
			xvfb-run Archi -application com.archimatetool.commandline.app -consoleLog -nosplash \
				--abortOnException --loadModel test/Export_Test.archimate --csv.export /tmp/ --csv.exportFilenamePrefix testing_

# Archi csv representation to internal
archicsv_2_int:
	$(CONTAINER) run -it --rm \
		--name $(CONTAINER_WEB_NAME) \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEMP_PATH):/tmp/:z \
		$(CONTAINER_WEB_IMAGE) \
		poetry run python csv_export_to_json.py test/export_elements.csv test/export_properties.csv test/export_relations.csv /tmp/export_output.json
# Alter internal representation
# Convert Back
int_2_archicsv:
	$(CONTAINER) run -it --rm \
		--name $(CONTAINER_WEB_NAME) \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEMP_PATH):/tmp/:z \
		$(CONTAINER_WEB_IMAGE) \
		poetry run python csv_import_from_json.py /tmp/export_output.json /tmp/import_node.csv /tmp/import_props.csv /tmp/import_links.csv

# Internal representation to flat node with props csv
int_2_flat:
	$(CONTAINER) run -it --rm \
		--name $(CONTAINER_WEB_NAME) \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEMP_PATH):/tmp/:z \
		$(CONTAINER_WEB_IMAGE) \
		poetry run python json_to_nodesprops.py /tmp/export_output.json /tmp/node_props.csv /tmp/link_props.csv

# Alter nodes and links
# Convert Back
flat_2_int:
	$(CONTAINER) run -it --rm \
		--name $(CONTAINER_WEB_NAME) \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEMP_PATH):/tmp/:z \
		$(CONTAINER_WEB_IMAGE) \
		poetry run python nodeprops_to_json.py node_props.csv link_props.csv import_output.json.json

translate_container:
	$(CONTAINER) run -it --rm \
		--name $(CONTAINER_WEB_NAME)-shell \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEMP_PATH):/tmp/:z \
		$(CONTAINER_WEB_IMAGE) \
			poetry run archi translate test/Export_Test.archimate /tmp/test.yml

test_archi_nodeprop:
	@mkdir -p ${TEST_PATH}
	@mkdir -p ${TEMP_PATH}
	$(CONTAINER) run -it --rm \
		--name $(CONTAINER_WEB_NAME)-test \
		-v $(SRC_PATH)/:/src/ \
		-v $(DATA_PATH):/data/:z \
		-v $(TEST_PATH):/test/:z \
		-v $(TEMP_PATH):/tmp/:z \
		$(CONTAINER_WEB_IMAGE) \
			poetry run python -m archi translate test/Export_Test.archimate /tmp/out.yaml

# translate:
# 	poetry run archi translate Export_Test.archimate test.yml