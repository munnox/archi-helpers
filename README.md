# Helper scripts for Archi

This is a simple start for processing data from the archimate system using the archi program and a python library. This can export from Archi and help to assemble data for import. This should allow better data model processing script to work on the intermediary.

Got a archimate model here [Model](Export_Test.archimate). This has a simple model in one view and one of all the element in another view. This is to get a idea of where each element needs to go in the folders.

Picture of the model is here:

![Export Test Model](Export_Test_Model.png)

# Running the Archi Processor

The Archi Processor is a container featuring an installation of Archi with scripts same as the archi container. Also this is a python container 

This container can be build locally by `make build_archi_processor` or is built in the gitlab.

```bash
sudo docker pull registry.gitlab.com/munnox/archi-helpers/archi-processor:latest

sudo docker run -it --rm -v $(pwd):/work -v $(pwd)/test:/test registry.gitlab.com/munnox/archi-helpers/archi-processor:latest bash
```

Then in the container:

```bash
# This will clone from the given repo with a API token
archi_clone_model https://gitlab.com/munnox/archi-example-ea-capabilities.git
archi_clone_model https://gitlab.com/munnox/archi-example-ansible-inventory.git

archi_clone_model <https_repo> <git_api_token>

# This will build a model from a colab repo
archi_repo_to_model ./repo/ test.archimate

# This is using the python library to understand and in this case translate the model into to yaml
archi translate model_file test.yaml

# This takes a model and output a templated result
archi template test/Export_Test.archimate archi/templating/default.html.j2 data/out_default.html
```

This is all now available as a container to process output as a CICD pipeline. For more examples see the `gitlab-ci.yaml`.

```yaml
variables:
  ARCHI_PROCESS_IMAGE: registry.gitlab.com/munnox/archi-helpers/archi-processor:latest
  ARCHI_REPO_IMAGE: https://gitlab.com/munnox/archi-example-ea-capabilities.git

pages:
  image: $ARCHI_PROCESS_IMAGE
  stage: test
  script:
    - archi_clone_model $ARCHI_REPO_IMAGE
    - mkdir public
    - archi template cloned.archimate /src/archi/templating/default.html.j2 public/archi_report.html
    - archi_conv_report cloned.archimate public/report
    - archi_conv_csv cloned.archimate public export_
    - |
      cat << EOF > public/index.html
      <html>
        <head></head>
        <body>
          <div><a href="report/index.html">Report</a></div>
          <div><a href="archi_report.html">RAM Templated Report using python library</a></div>
          <div><a href="export_elements.csv">Model export to CSV - Elements</a></div>
          <div><a href="export_properties.csv">Model export to CSV - Properties</a></div>
          <div><a href="export_relations.csv">Model export to CSV - Relations</a></div>
        </body>
      </html>
      EOF
  artifacts:
    paths:
      - public
```

## Archi Hub

A Web service in django to allow the web server to server archi models.

This uses the processor and other tool and allows the upload to Neo4j to drive better analysis and reporting.

Untested lately dues to lack of time. getting more use out of the archi processor container and scripts and integrating the Archimate Python library `archi`.

# Development and testing

## Archi Processor

```bash
make build_archi_processor

make shell_archi_processor
```

Then within the shell processor container:

```bash
archi_clone_model https://gitlab.com/munnox/archi-example-ea-capabilities.git


archi template /test/Export_Test.archimate /src/archi/templating/default.html.j2 /data/out_default.html
archi template /test/Export_Test.archimate /src/archi/templating/default_node_rel.html.j2 /data/out_default.html

# Render Model data as a html with bootstrap formating
archi template /test/Export_Test.archimate /src/archi/templating/default_bootstrap_model_nodes.html.j2 /data/out_default.html


# Render Model data as a html with bootstrap formating with test plugins
archi template /test/Export_Test.archimate /src/archi/templating/default.yaml.j2 test.yaml --plugin-path /test/plugins/

# Testing
archi template /test/Export_Test.archimate /test/test_default.html.j2 /temp/test.html --plugin-path /test/plugins/
```


# Translate information

## Model manipulation using the CSV export and import

```
# Export model from Archi to 3 CSV's
archi -[export as csv]-> [.csv]
# Convert CSV to json intermediate
[.csv] -[csv_export_to_json.py]-> [.json]
# Convert json intermediate to Node and Link property CSVs
[.json] -[json_to_nodesprops.py]-> [.csv]

# Go edit it
[.csv] -[edit in excel]-> [.csv]

# Convert Node and Link property CSVs back to json intermediate
[.csv] -[nodeprops_to_json.py]-> [.json]
# Convert JSON internmediate to the 3 Archi CSVs
[.json] -[json_to_csv_import.py]-> [.csv]
# Import the updated 3 CSV Files
[.csv] -[import from csv]-> archi
```

```bash
# Get to a poetry shell to run script with python dependances
python3 -m poetry shell

# From the exported 3 CSV's to a JSON data file for further processing
python csv_export_to_json.py export_elements.csv export_properties.csv export_relations.csv export_output.json
# From the JSON data file to the 3 CSV's ready for import
python csv_import_from_json.py export_output.json import_node.csv import_props.csv import_links.csv 

# Further processing of the json

# From json build a node and link csv with properties as columns
python json_to_nodesprops.py export_output.json node_props.csv link_props.csv

# Recombine the nodes and links with properties back into a JSON
python nodeprops_to_json.py node_props.csv link_props.csv import_output.json.json
```

## Process a archimate file directly

```bash
# Process xml or archimate archive to dict dumping a yaml and rebuild the xml
python read_xml_archi.py Export_Test.archimate test.yaml
# and now added __main__.py the following will run the main code block
python archi translate Export_Test.archimate test.yaml
# Or with Poetry run script
poetry run archi translate Export_Test.archimate test.yml

poetry run python manage.py runserver_plus --cert-file /tmp/cert.crt 0.0.0.0:8000
```

## Edit and adjust the VueJS application

Runs a docker container to allow interactive Vuejs Vite application to speed UI development.

```bash
make dev_app_vue
```

When in the container run:

```bash
# Install npm dependancies
npm install
# Run the dev server with a 0.0.0.0 bind to allow it work well within a container
npm run dev_net
```
