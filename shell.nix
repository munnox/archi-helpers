{ pkgs ? import <nixpkgs> {} # here we import the nixpkgs package set
}:
let
  # nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/nixos-22.11";
  # pkgs = import nixpkgs { config = {}; overlays = []; };

  runpython = pkgs.writeScriptBin "python" ''
    echo "Running python with further args \"''${@:1}\""
    ${pkgs.poetry}/bin/poetry run python ''${@:1}
  '';

  basepkgs = [
    pkgs.poetry
  ];

  scripthelpers = [runpython];

in pkgs.mkShell rec {
  name="Archi Scripts Development Environment";    # that requires a name
  buildInputs =  basepkgs ++ scripthelpers;

  shellHook = ''
    # bash to run when you enter the shell     
    echo "Start developing..."
    # source "$HOME/.bashrc"
    export PS1=$txtred"NIX✅$txtrst-"$txtblu"${name}"$txtrst"\n$PS1"
    # poetry install
    # poetry shell

  '';               
}