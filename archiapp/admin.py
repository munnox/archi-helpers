from django_neomodel import admin as neo_admin
from django.contrib import admin
from .models import Element

# Register your models here.
class ElementAdmin(admin.ModelAdmin):
    list_display = (
        "uid",
        "aid",
        "name",
        "type",
        "specialisation",
        "documentation",
        "properties",
        "folders",
    )


neo_admin.register(Element, ElementAdmin)
