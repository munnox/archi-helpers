from django import forms

from .models import Element

# https://docs.djangoproject.com/en/4.2/topics/http/file-uploads/
class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField(
        label="File Upload",
        # widget=forms.ClearableFileInput(attrs={'multiple': True})
    )


class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True


class MultipleFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = single_file_clean(data, initial)
        return result


class FileFieldForm(forms.Form):
    CHOICES = (
        ("erp", "Elements, Relationships and Properties"),
        ("el", "Flat Classed Elements with Relationships"),
        ("view", "Export View CSV"),
        ("model", "Archimate Model ready for import"),
        ("elsep", "Flat Classed Elements separated with Relationships"),
        ("json", "Internal Model JSON"),
        ("xml", "XML,YAML,Internal Model YAML"),
        ("clear_neo", "Clear the Neo4J Database"),
        ("to_neo", "Place Elements and relationship into Neo4j"),
        ("map_types", "Map Internal Types to Archimate format in Neo4j"),
        (
            "process_erp",
            "Requires Archi Process - Elements, Relationships and Properties Archi",
        ),
        ("process_report", "Requires Archi Process - HTML Report"),
    )
    prefix = forms.CharField(label="Prefix", max_length=50)
    output_format = forms.MultipleChoiceField(
        label="Output Format", choices=CHOICES, widget=forms.CheckboxSelectMultiple()
    )
    file_field = MultipleFileField()


# class ElementForm(forms.ModelForm):
#     class Meta:
#         model = Element
#         fields = ["uid", "aid", "name", "type", "specialisation", "documentation", "properties", "folders"]
