from datetime import datetime
import logging
import os
import uuid
import zipfile
import subprocess
import glob
import textwrap
from django.shortcuts import render, redirect

from django.conf import settings

from django.http import (
    HttpResponse,
    HttpResponseRedirect,
    FileResponse,
    HttpResponseNotFound,
)
from django.template import loader

from archi import storage
from archi.errors import ArchiError
from archi.models import Archimodel, Element as Archi_Element
from .models import Element, Relation, clear_neo4j, map_elements

from .forms import FileFieldForm

from archi.converters import (
    convert_csv_export_to_json,
    convert_json_nodeprops,
    # convert_links_specialisation_groups_to_nodeprops,
    # convert_model_links_to_special_groups,
    convert_model_element_to_special_groups,
    convert_elements_specialisation_groups_to_nodeprops,
    convert_to_csv,
    nodeprops_to_json,
    json_csv_import,
)

logger = logging.getLogger(__name__)


# Create your views here.
def index(request):
    template = loader.get_template("archiapp/index.html")
    context = {"uploadform": FileFieldForm()}

    return HttpResponse(template.render(context, request))
    # return render(request, "content.html")


# Model upload form
def upload_file(request):
    render_variables = {"uploadform": None, "data": None}
    if request.method == "POST":
        uploadform = FileFieldForm(request.POST, request.FILES)
        render_variables["uploadform"] = uploadform
        files = request.FILES.getlist("file_field")
        if uploadform.is_valid():
            prefix = uploadform.cleaned_data[
                "prefix"
            ]  # .replace(" ", "_").replace("\n", "")
            output_format = uploadform.cleaned_data["output_format"]
            logger.info(f"Form Prefix: {prefix} Format: {output_format}")
            results = do_upload_conversion(prefix, output_format, files)
            if type(results) == type(HttpResponse()):
                return results
            render_variables["results"] = results
    else:
        render_variables["uploadform"] = FileFieldForm()
    return render(request, "archiapp/upload.html", render_variables)
    # return render(request, "archiapp/upload.html", {"uploadform": uploadform, "data": request.FILES["file"]})


def handle_uploaded_file(f):
    file_path = os.path.join(settings.MEDIA_ROOT, f.name)
    logger.info(f"Filepath : {file_path}")
    with open(file_path, "wb+") as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return file_path


def serve_report(request, path):
    file_path = os.path.join(settings.MEDIA_ROOT, "reports", path)
    file_exists = os.path.exists(file_path)
    length = None
    with open(file_path, "rb") as f:
        length = len(f.read())
    logger.info(
        f"path {path} file_path {file_path} Exists: {file_exists} Length: {length}"
    )

    try:
        response = FileResponse(open(file_path, "rb"))  # \, content_type="text/csv")
        # https://docs.djangoproject.com/en/1.11/howto/outputting-csv/#streaming-large-csv-files
        # response['Content-Disposition'] = 'attachment; filename="%s"'%filename

        # with open(file_path, 'r') as f:
        #    file_data = f.read()

        # # sending response
        # response = HttpResponse(file_data) #, content_type='application/vnd.ms-excel')
    #     response['Content-Disposition'] = 'attachment; filename="foo.xls"'

    except IOError:
        # handle file not exist case here
        response = HttpResponseNotFound("<h1>File not exist</h1>")
    return response


# def archi_upload(request):
#     template = loader.get_template("archiapp/upload")
#     context = {}

#     return HttpResponse(template.render(context, request))
#     # return render(request, "content.html")
#     return redirect()


def do_upload_conversion(prefix, output_format, files):
    results = None
    elements_csv = None
    properties_csv = None
    relations_csv = None
    input_nodes_csv = None
    input_links_csv = None
    input_views_csv = None
    input_archi = None
    for f in files:
        logger.info(f"File name: {f.name}, {settings.MEDIA_ROOT}")
        final_path = handle_uploaded_file(f)
        if "elements" in f.name:
            elements_csv = final_path
        if "properties" in f.name:
            properties_csv = final_path
        if "relations" in f.name:
            relations_csv = final_path
        if "nodes" in f.name:
            input_nodes_csv = final_path
        if "links" in f.name:
            input_links_csv = final_path
        if "view" in f.name:
            input_views_csv = final_path
        if ".archimate" in f.name:
            input_archi = final_path
        if ".zip" in f.name:
            input_archi = final_path

    output_zip = os.path.join(settings.MEDIA_ROOT, "output.zip")
    export_files = []

    # If given the export csv from archi generate the node props format with links
    if elements_csv is not None:
        logger.info(
            f"Flatten Elements, Properties and Relationships file suspected '{prefix}', Elements: {elements_csv} Properties: {properties_csv} Relations: {relations_csv}, output: {output_format}"
        )
        process_ele_rel_prop(
            output_format, elements_csv, properties_csv, relations_csv, export_files
        )

    # If given Node props file and a link file and generate the importable csv files
    if input_nodes_csv is not None:
        logger.info(
            f"Flattened Node and Link file suspected '{prefix}', Nodes: {input_nodes_csv} Links: {input_links_csv}, output: {output_format}"
        )
        process_node_link(output_format, input_nodes_csv, input_links_csv, export_files)

    # If given Node props file and a link file and generate the importable csv files
    if input_views_csv is not None:
        logger.info(
            f"Turn a view export file suspected '{prefix}', Views: {input_views_csv} output: {output_format}"
        )
        process_views(output_format, input_views_csv, export_files)

    # If given Archi model file convert to csv export files
    if input_archi is not None:
        logger.info(
            f"Archimate file suspected '{prefix}', Model: {input_archi}, output: {output_format}"
        )
        results = process_archi_model(prefix, output_format, input_archi, export_files)

    if len(export_files) > 0:
        logger.info("Packaging - Building ZIP File")
        logger.info(f"Exported files: {export_files}")
        with zipfile.ZipFile(output_zip, "w") as zipped_f:
            for exfile, infile in export_files:
                zipped_f.write(exfile, infile)

        logger.info(
            "Cleaning - Removing file use and created just leaving the export zip file"
        )
        for exfile, infile in export_files:
            file_path = exfile
            if os.path.isfile(file_path):
                os.remove(file_path)

        logger.info("Sending - Attaching ZIP file")
        zip_file = open(output_zip, "rb")
        response = HttpResponse(zip_file, content_type="application/force-download")
        response["Content-Disposition"] = 'attachment; filename="%s"' % "output.zip"
        return response
    # Return None to continue processing result
    return results


def process_node_link(output_format, input_nodes_csv, input_links_csv, export_files):
    output_json = "output.json"
    ele_csv = "out_elements.csv"
    rel_csv = "out_relations.csv"
    props_csv = "out_properties.csv"

    logger.info("Processing - Node Props format with link to json format")
    nodeprops_to_json(
        input_nodes_csv,
        input_links_csv,
        os.path.join(settings.MEDIA_ROOT, output_json),
        column_separator="|",
        class_type_separator="|",
        property_separator="|",
    )

    logger.info("Processing - JSON to CSV's for import")
    json_csv_import(
        os.path.join(settings.MEDIA_ROOT, output_json),
        os.path.join(settings.MEDIA_ROOT, ele_csv),
        os.path.join(settings.MEDIA_ROOT, rel_csv),
        os.path.join(settings.MEDIA_ROOT, props_csv),
    )

    if "erp" in output_format:
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, ele_csv), ele_csv],
        )
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, rel_csv), rel_csv],
        )
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, props_csv), props_csv],
        )
    if "json" in output_format:
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, output_json), output_json],
        )


def process_views(output_format, input_views_csv, export_files):
    output_json = "output.json"
    model_xml = "out_model.xml"
    result_path = os.path.join(settings.MEDIA_ROOT, output_json)

    logger.info("Processing - Node Props format with link to json format")
    raise NotImplemented("Not implemented yet")

    logger.info("Processing - JSON to CSV's for import")

    if "model" in output_format:
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, model_xml), model_xml],
        )
    if "json" in output_format:
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, output_json), output_json],
        )


def process_ele_rel_prop(
    output_format, elements_csv, properties_csv, relations_csv, export_files
):
    output_json = "output.json"
    nodes_csv = "nodes.csv"
    links_csv = "links.csv"
    # Step 1 convert to json
    logger.info("Processing - Converting csv export to JSON")
    convert_csv_export_to_json(
        elements_csv,
        properties_csv,
        relations_csv,
        os.path.join(settings.MEDIA_ROOT, output_json),
    )

    # Step 2 Convert json to Node properties format with links
    logger.info("Processing - Converting from JSON to Node property format")
    properties, properties_key_set, classes = convert_json_nodeprops(
        os.path.join(settings.MEDIA_ROOT, output_json),
        os.path.join(settings.MEDIA_ROOT, nodes_csv),
        os.path.join(settings.MEDIA_ROOT, links_csv),
        embed_links=False,
        property_separator="|",
        class_type_separator="|",
        column_separator="|",
        default_property_value=None,
    )

    if "el" in output_format:
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, nodes_csv), nodes_csv],
        )
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, links_csv), links_csv],
        )
    if "json" in output_format:
        export_files.append(
            [os.path.join(settings.MEDIA_ROOT, output_json), output_json],
        )


def all_hash(node, special):
    return ("nodes",)


def process_archi_model(prefix, output_format, input_archi, export_files):

    results = None

    if "clear_neo" in output_format:
        # TODO: Clear the neoj elements
        clear_neo4j()

    if "to_neo" in output_format:
        archi_model = Archimodel.from_archifile(input_archi)

        def get_create(e: Element):
            node = Element.nodes.get_or_none(aid=e.aid)
            if node is None:
                node = e
                node.save()
            return node

        e: Archi_Element
        # elements = [
        #     for k in archi_model.elements

        # ]
        map_types = "map_types" in output_format

        logger.info(f"Upload Model to Neo4j")
        nodes = {}
        for node in archi_model.elements:
            try:
                special = archi_model.profiles[node.Specialization]
                special_name = special.Name
            except KeyError:
                special_name = ""

            # NOTE: Folder Type do not have a Type field
            # The issues is should they right now I don't not think so
            # they are not a normal element with a type
            try:
                node_type = node.Type
            except:
                node_type = node.__class__.__name__

            base_element = {
                "uid": uuid.uuid4(),
                "aid": node.ID,
                "name": node.Name,
                "type": node_type,
                "specialisation": special_name,
                "properties": node.Properties,
                "folders": node.Folder,
                "documentation": node.Documentation,
                "created": datetime.now(),
            }
            C = Element
            if map_types:
                try:
                    C = map_elements[base_element["type"]]
                except KeyError:
                    pass
            e = C(**base_element)
            neo_node = get_create(e)
            nodes[neo_node.aid] = neo_node

        for l in archi_model.links:
            src = nodes[l.Source]
            tgt = nodes[l.Target]
            try:
                special = archi_model.profiles[l.Specialization]
                special_name = special.Name
            except KeyError:
                special_name = ""
            r = {
                "aid": l.ID,
                "name": l.Name,
                "type": l.Type,
                "specialisation": special_name,
                "documentation": l.Documentation,
                "created": datetime.now(),
            }
            if l.Type == "CompositionRelationship" and map_types:
                rel = src.composition.connect(tgt, r)
            elif l.Type == "AggregationRelationship" and map_types:
                rel = src.aggregation.connect(tgt, r)
            elif l.Type == "AssignmentRelationship" and map_types:
                rel = src.assignment.connect(tgt, r)
            elif l.Type == "RealizationRelationship" and map_types:
                rel = src.realisation.connect(tgt, r)
            elif l.Type == "ServingRelationship" and map_types:
                rel = src.serving.connect(tgt, r)
            elif l.Type == "AccessRelationship" and map_types:
                rel = src.access.connect(tgt, r)
            elif l.Type == "InfluenceRelationship" and map_types:
                rel = src.influence.connect(tgt, r)
            elif l.Type == "AssociationRelationship" and map_types:
                rel = src.association.connect(tgt, r)
            elif l.Type == "TriggeringRelationship" and map_types:
                rel = src.triggering.connect(tgt, r)
            elif l.Type == "FlowRelationship" and map_types:
                rel = src.flow.connect(tgt, r)
            elif l.Type == "SpecializationRelationship" and map_types:
                rel = src.specialised.connect(tgt, r)
            elif l.Type == "JunctionRelationship" and map_types:
                rel = src.junction.connect(tgt, r)
            else:
                rel = src.link.connect(tgt, r)

        logger.info(f"Finished Uploading Model to Neo4j")

        # node1 = Element(uid="test_id1", aid="id-1", name="to_neo1", type="Node", specialisation=None, documentation="Node 1 Docs", created=datetime.now())
        # node1 = get_create(node1)

        # node2 = Element(uid="test_id2", aid="id-2", name="to_neo2", type="Node", specialisation=None, documentation="Node 2 Docs", created=datetime.now())
        # node2 = get_create(node2)
        # # node2 = Element.nodes.get_or_none(uid='test_id2')
        # # if node2 is None:
        # #     node2 = Element(uid="test_id2", aid="id-2", name="to_neo2", type="Node", specialisation=None, documentation="Node 2 Docs", created=datetime.now())
        # #     node2.save()

        # node1.link.connect(node2, {"aid": "rel1", "name": "", "type": "realisation", "specialisation": "", "documentation": "", "created": datetime.now()})

        # rel = Relationship(uid="test_id", name="", type="realisation", specialisation=None, documentation="", created=datetime.now())
        # rel.save()

    if "xml" in output_format:
        logger.info("produce XML")
        # try:
        #     archi_dict_model = storage.read_archi_file(input_archi)
        # except Exception as error:
        #     error_msg = f"File load error -> {error}"
        #     logger.error(error_msg)
        #     raise ArchiError(error_msg) from error
        #     # return

        # logger.info(f"Model keys: {archi_dict_model.keys()} {archi_dict_model['archimate:model'].keys()} {archi_dict_model['archimate:model']['profile']}")
        archi_model = Archimodel.from_archifile(input_archi)
        dict_model = archi_model.to_dict()

        # dump_json(json_file, dict_model)
        yaml_internal_file = "archi_internal.yaml"
        export_files.append(
            (os.path.join(settings.MEDIA_ROOT, yaml_internal_file), yaml_internal_file)
        )
        storage.dump_yaml(
            os.path.join(settings.MEDIA_ROOT, yaml_internal_file), dict_model
        )

        yaml_file = "archi.yaml"
        export_files.append((os.path.join(settings.MEDIA_ROOT, yaml_file), yaml_file))
        storage.dump_yaml(
            os.path.join(settings.MEDIA_ROOT, yaml_file), archi_model.original_dict
        )

        xml_file = "archi.xml"
        export_files.append((os.path.join(settings.MEDIA_ROOT, xml_file), xml_file))
        storage.write_xml_file(
            os.path.join(settings.MEDIA_ROOT, xml_file), archi_model.original_dict
        )

        # logger.info(f"Write output files: /tmp/elements_types.csv /tmp/relations.csv")
        logger.info("XML Done")

    if "view" in output_format:

        archi_model = Archimodel.from_archifile(input_archi)

        view_elements = []

        for ele in archi_model.views:
            for k in ele.children:
                child_dict = ele.children[k].to_dict()
                logger.info(f"Diagram object: {child_dict}")
                view_elements.append(
                    {
                        "viewid": ele.ID,
                        "folder": child_dict["Folder"],
                        "viewname": ele.Name,
                        "id": child_dict["ID"],
                        "type": child_dict["Type"],
                        "name": child_dict["Name"],
                        "modelid": child_dict.get("modelid", None),
                        "x": child_dict["x"],
                        "y": child_dict["y"],
                        "width": child_dict["width"],
                        "height": child_dict["height"],
                        "label": child_dict["label"],
                        "content": child_dict["content"],
                        "documentation": child_dict["Documentation"],
                        "properties": child_dict["Properties"],
                        "connections": child_dict["connections"],
                        "other_dict": child_dict,
                    }
                )
        results = view_elements
        file_name = f"views.csv"
        export_files.append((os.path.join(settings.MEDIA_ROOT, file_name), file_name))
        convert_to_csv(
            os.path.join(settings.MEDIA_ROOT, file_name),
            view_elements,
            [
                "viewid",
                "folder",
                "viewname",
                "id",
                "type",
                "name",
                "modelid",
                "x",
                "y",
                "width",
                "height",
                "label",
                "content",
                "documentation",
                "properties",
                "connections",
            ]
            #     "Type",
            #     "Specialization",
            #     "Documentation",
            #     "Source",
            #     "Target",
            #     "Class_Name",
            #     "Class_Columns",
            # ],
        )

    if "elsep" in output_format:

        archi_model = Archimodel.from_archifile(input_archi)
        csv_dict_model_nodes = archi_model.convert_separate_tables_nodes()
        csv_dict_model_links = archi_model.convert_separate_tables_links()

        logger.info(f"Write output files: /tmp/elements_types.csv /tmp/relations.csv")

        special_group_nodes = csv_dict_model_nodes["specialisation_groups"]
        # export_files = []
        for specialisation_key in special_group_nodes:
            # if type(specialisation_key) != type(tuple()):
            #     specialisation_key = tuple([specialisation_key])
            file_safe_specialisation = [
                ele.replace("/", "_").replace(":", "_") for ele in specialisation_key
            ]
            file_name = f"nodes_{'-'.join(file_safe_specialisation)}.csv"
            export_files.append(
                (os.path.join(settings.MEDIA_ROOT, file_name), file_name)
            )
            convert_to_csv(
                os.path.join(settings.MEDIA_ROOT, file_name),
                special_group_nodes[specialisation_key]["aligned_elements"],
                [
                    "ID",
                    "Name",
                    "Type",
                    "Specialization",
                    "Documentation",
                    "Class_Name",
                    "Class_Columns",
                ],
            )

        special_group_links = csv_dict_model_links["specialisation_groups"]
        # export_files = []
        for specialisation_key in special_group_links:
            # if type(specialisation_key) != type(tuple()):
            #     specialisation_key = tuple([specialisation_key])
            file_safe_specialisation = [
                ele.replace("/", "_").replace(":", "_") for ele in specialisation_key
            ]
            file_name = f"links_{'-'.join(file_safe_specialisation)}.csv"
            export_files.append(
                (os.path.join(settings.MEDIA_ROOT, file_name), file_name)
            )
            convert_to_csv(
                os.path.join(settings.MEDIA_ROOT, file_name),
                special_group_links[specialisation_key]["aligned_elements"],
                [
                    "ID",
                    "Name",
                    "Type",
                    "Specialization",
                    "Documentation",
                    "Source",
                    "Target",
                    "Class_Name",
                    "Class_Columns",
                ],
            )

        # rel_csv = "links.csv"
        # export_files.append((os.path.join(settings.MEDIA_ROOT, rel_csv), rel_csv))
        # convert_to_csv(
        #     os.path.join(settings.MEDIA_ROOT, rel_csv),
        #     csv_dict_model_nodes["links"],
        #     [
        #         "ID",
        #         "Name",
        #         "Type",
        #         "Specialization",
        #         "Source",
        #         "Target",
        #         "Folder",
        #         "Documentation",
        #     ],
        # )

    # ====== The following option using process_ is using the Archi Moddeling tool built in to the container =====
    # This allow the helper to do more inbuilt model manipulation.
    # Using the Archi internal tools and code

    if "process_erp" in output_format:
        result_csv = subprocess.run(
            [
                "conv_csv.sh",
            ],
            env={
                "DISPLAY": ":99",
                "ARCHI_FILE": input_archi,
                "EXPORT_PATH": settings.MEDIA_ROOT,
                "PREFIX": prefix,
            },
            shell=True,  # check=True,
            capture_output=True,
        )
        logger.info(f"Process Result\n\n{result_csv}")

        # output_zip = "/tmp/output.zip"

        ele_csv = f"{prefix}elements.csv"
        rel_csv = f"{prefix}relations.csv"
        props_csv = f"{prefix}properties.csv"
        export_files.append([os.path.join(settings.MEDIA_ROOT, ele_csv), ele_csv])
        export_files.append([os.path.join(settings.MEDIA_ROOT, rel_csv), rel_csv])
        export_files.append([os.path.join(settings.MEDIA_ROOT, props_csv), props_csv])
        # ]

    if "process_report" in output_format:
        report_base_path = os.path.join(settings.MEDIA_ROOT, "reports")
        report_path = os.path.join(report_base_path, prefix)
        try:
            os.mkdir(report_path)
        except:
            pass

        result_report = subprocess.run(
            [
                "conv_report.sh",
            ],
            env={
                "DISPLAY": ":99",
                "ARCHI_FILE": input_archi,
                "EXPORT_PATH": report_path,
            },
            shell=True,  # check=True,
            capture_output=True,
        )
        logger.info(f"Process Result\n\n{result_report}")

        report_files = []
        for name in glob.glob(os.path.join(report_path, "*")):
            report_files.append(
                (name, name.replace(report_base_path + "/", f"report/"))
            )
            export_files.append(
                (name, name.replace(report_base_path + "/", f"report/"))
            )
        # result = subprocess.Popen(main_cmd + archi_args,  stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        # logger.info(f"Result Popen: {result.communicate()[1].decode('utf-8')}")
        # result.wait()
        result = textwrap.dedent(
            f"""
            Convert to report:
            Return Code: {result_report.returncode}

            Stdout:
            {textwrap.indent(result_report.stdout.decode("utf-8"), " " * 25)}
            
            Stderr:
            {textwrap.indent(result_report.stderr.decode("utf-8"), " " * 25)}

            {report_files}

            {export_files}
            """
        )

        report_file = "report.txt"
        export_files.append(
            (os.path.join(settings.MEDIA_ROOT, report_file), "report/" + report_file)
        )
        with open(os.path.join(settings.MEDIA_ROOT, report_file), "w") as f:
            f.write(result)

        logger.info(f"Export files: {export_files}")

    return results
