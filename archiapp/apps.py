from django.apps import AppConfig


class ArchiappConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "archiapp"
