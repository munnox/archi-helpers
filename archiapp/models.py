from django.db import models
from neomodel import (
    db,
    StructuredNode,
    StringProperty,
    DateProperty,
    StructuredRel,
    UniqueIdProperty,
    RelationshipFrom,
    RelationshipTo,
    Relationship,
    clear_neo4j_database,
    remove_all_labels,
    install_all_labels,
)
from django_neomodel import DjangoNode


def clear_neo4j():
    clear_neo4j_database(db)
    remove_all_labels()
    install_all_labels()


# Create your models here.


class Relation(StructuredRel):
    aid = StringProperty(unique_index=True)
    name = StringProperty()
    type = StringProperty()
    specialisation = StringProperty()
    documenation = StringProperty()
    properties = StringProperty()
    created = DateProperty()


class Element(DjangoNode):
    uid = UniqueIdProperty(primary_key=True)
    aid = StringProperty(unique_index=True)
    name = StringProperty()
    type = StringProperty()
    specialisation = StringProperty()
    documentation = StringProperty()
    properties = StringProperty()
    folders = StringProperty()
    created = DateProperty()
    parents = RelationshipFrom("Element", "LINK", model=Relation)
    children = RelationshipTo("Element", "LINK", model=Relation)
    link = Relationship("Element", "LINK", model=Relation)
    # Structural
    composition = Relationship("Element", "COMPOSITION", model=Relation)
    aggregation = Relationship("Element", "AGGREGRATION", model=Relation)
    assignment = Relationship("Element", "ASSIGNMENT", model=Relation)
    # assignment = Relationship("Element", "ASSIGNMENT", model=Relation)
    realisation = Relationship("Element", "REALISATION", model=Relation)
    # Dependacy
    serving = Relationship("Element", "SERVING", model=Relation)
    access = Relationship("Element", "ACCESS", model=Relation)
    influence = Relationship("Element", "INFLUENCE", model=Relation)
    association = Relationship("Element", "ASSOCIATION", model=Relation)
    # Dynamic
    triggering = Relationship("Element", "TRIGGERING", model=Relation)
    flow = Relationship("Element", "FLOW", model=Relation)
    # Other
    specialised = Relationship("Element", "SPECIALISATION", model=Relation)
    # Relationship
    junction = Relationship("Element", "JUNCTION", model=Relation)

    class Meta:
        app_label = "archiapp"


# From https://github.com/RemcoSchellekensNS/Archimate/blob/main/Archimate3.2%20Reference%20sheets.archimate
# Also saved in the test folder 2023_12_10


class Composite(Element):
    pass


class Passive(StructuredNode):
    pass


class Active(StructuredNode):
    pass


class Behaviour(StructuredNode):
    pass


class Motorvational(StructuredNode):
    pass


# Business
class Business(Element):
    pass


class BusinessActor(Business, Active):
    pass


class BusinessRole(Business, Active):
    pass


class BusinessCollaboration(Business, Active):
    pass


class BusinessInterface(Business, Active):
    pass


class BusinessProcess(Business, Behaviour):
    pass


class BusinessFunction(Business, Behaviour):
    pass


class BusinessInteraction(Business, Behaviour):
    pass


class BusinessEvent(Business, Behaviour):
    pass


class BusinessService(Business, Behaviour):
    pass


class BusinessObject(Business, Passive):
    pass


class Contract(Business, Passive):
    pass


class Representation(Business, Passive):
    pass


class Product(Business, Composite):
    pass


# Application
class Application(Element):
    pass


class ApplicationComponent(Application, Active):
    pass


class ApplicationCollaboration(Application, Active):
    pass


class ApplicationInterface(Application, Active):
    pass


class ApplicationFunction(Application, Behaviour):
    pass


class ApplicationInteraction(Application, Behaviour):
    pass


class ApplicationProcess(Application, Behaviour):
    pass


class ApplicationEvent(Application, Behaviour):
    pass


class ApplicationService(Application, Behaviour):
    pass


class DataObject(Application, Passive):
    pass


# Technology
class Technology(Element):
    pass


class Node(Technology, Active):
    pass


class Device(Technology, Active):
    pass


class SystemSoftware(Technology, Active):
    pass


class TechnologyCollaboration(Technology, Active):
    pass


class TechnologyInterface(Technology, Active):
    pass


class Path(Technology, Active):
    pass


class CommunicationNetwork(Technology, Active):
    pass


class TechnologyFunction(Technology, Behaviour):
    pass


class TechnologyProcess(Technology, Behaviour):
    pass


class TechnologyInteraction(Technology, Behaviour):
    pass


class TechnologyEvent(Technology, Behaviour):
    pass


class TechnologyService(Technology, Behaviour):
    pass


class Artifact(Technology, Passive):
    pass


class Equipment(Technology, Active):
    pass


class Facility(Technology, Active):
    pass


class DistributionNetwork(Technology, Active):
    pass


class Material(Technology, Active):
    pass


# Strategy
class Strategy(Element):
    pass


class Resource(Strategy, Active):
    pass


class Capability(Strategy, Behaviour):
    pass


class ValueStream(Strategy, Behaviour):
    pass


class CourseOfAction(Strategy, Behaviour):
    pass


# Motivation
class Motivation(Element):
    pass


class Stakeholder(Motivation):
    pass


class Driver(Motivation):
    pass


class Assessment(Motivation):
    pass


class Goal(Motivation):
    pass


class Outcome(Motivation):
    pass


class Principle(Motivation):
    pass


class Requirement(Motivation):
    pass


class Constraint(Motivation):
    pass


class Meaning(Motivation):
    pass


class Value(Motivation):
    pass


# Implemention and Migration
class Implementation(Element):
    pass


class WorkPackage(Implementation):
    pass


class Deliverable(Implementation):
    pass


class ImplementationEvent(Implementation):
    pass


class Plateau(Implementation):
    pass


class Gap(Implementation):
    pass


# Composite


class Grouping(Composite):
    pass


class Location(Composite):
    pass


map_elements = {
    # Business
    "BusinessActor": BusinessActor,
    "BusinessRole": BusinessRole,
    "BusinessCollaboration": BusinessCollaboration,
    "BusinessInterface": BusinessInterface,
    "BusinessProcess": BusinessProcess,
    "BusinessFunction": BusinessFunction,
    "BusinessInteraction": BusinessInteraction,
    "BusinessEvent": BusinessEvent,
    "BusinessService": BusinessService,
    "BusinessObject": BusinessObject,
    "Contract": Contract,
    "Representation": Representation,
    "Product": Product,
    # Application
    "ApplicationComponent": ApplicationComponent,
    "ApplicationCollaboration": ApplicationCollaboration,
    "ApplicationInterface": ApplicationInterface,
    "ApplicationFunction": ApplicationFunction,
    "ApplicationInteraction": ApplicationInteraction,
    "ApplicationProcess": ApplicationProcess,
    "ApplicationEvent": ApplicationEvent,
    "ApplicationService": ApplicationService,
    "DataObject": DataObject,
    # Technology
    "Node": Node,
    "Device": Device,
    "SystemSoftware": SystemSoftware,
    "TechnologyCollaboration": TechnologyCollaboration,
    "TechnologyInterface": TechnologyInterface,
    "Path": Path,
    "CommunicationNetwork": CommunicationNetwork,
    "TechnologyFunction": TechnologyFunction,
    "TechnologyProcess": TechnologyProcess,
    "TechnologyInteraction": TechnologyInteraction,
    "TechnologyEvent": TechnologyEvent,
    "TechnologyService": TechnologyService,
    "Artifact": Artifact,
    "Equipment": Equipment,
    "Facility": Facility,
    "DistributionNetwork": DistributionNetwork,
    "Material": Material,
    # Motivation
    "Stakeholder": Stakeholder,
    "Driver": Driver,
    "Assessment": Assessment,
    "Goal": Goal,
    "Outcome": Outcome,
    "Principle": Principle,
    "Requirement": Requirement,
    "Constraint": Constraint,
    "Meaning": Meaning,
    "Value": Value,
    # Strategy
    "Resource": Resource,
    "Capability": Capability,
    "ValueStream": ValueStream,
    "CourseOfAction": CourseOfAction,
    # Implementation and Migration
    "WorkPackage": WorkPackage,
    "Deliverable": Deliverable,
    "ImplementationEvent": ImplementationEvent,
    "Plateau": Plateau,
    "Gap": Gap,
    # Composite
    "Grouping": Grouping,
    "Location": Location,
}
