import { fileURLToPath, URL } from 'node:url'

import { normalizePath, defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

console.log(`Meta: ${import.meta.url} ${fileURLToPath(new URL('./src', import.meta.url))}`)

// import.meta.env is not defined here this ref is within the main.ts
// console.log(`ENV: ${import.meta.env}`)
console.log(`import.meta`, import.meta);
// console.log(`import.meta.env`, import.meta.env);
console.log(`process.env`, process.env);
console.log(`ENV __APP_VERSION__: ${process.env.__APP_VERSION__ || "development"}`)

console.log("End of vite.config.ts")
// https://vitejs.dev/config/
/** @type {import('vite').defineConfig} */
export default defineConfig({
  base: `/vue/`,
  define: {
    "__APP_VERSION__": `"${process.env.__APP_VERSION__ || "development"}"`,
  },
  plugins: [
    vue(),
    vueJsx(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    rollupOptions: {
      output: {
        assetFileNames(assetInfo: any) {
          // output src/assets/locales/*.json files to dist/locales
          const pathToFile = normalizePath(assetInfo.name)
          if (/\/src\/assets\/.*\.json$/.test(pathToFile)) {
            return 'assets/[name].json'
          }

          return 'assets/[name]-[hash][extname]'
        },
      },
    },
  },
})
