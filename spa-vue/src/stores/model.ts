import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useModelStore = defineStore('model', () => {
  // other options...
  const nodes = ref([])
  const links = ref([])
  const profiles = ref([])

  const get_nodes = computed(() => {
    return nodes
  })
  const get_links = computed(() => {
    return links
  })
  const get_profiles = computed(() => {
    return profiles
  })

  function set_model(model: any) {
    nodes.value = model.nodes
    links.value = model.links
    profiles.value = model.profiles
  }
  return { nodes, links, profiles, get_nodes, get_links, get_profiles, set_model }
})
