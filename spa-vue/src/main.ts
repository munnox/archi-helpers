import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

console.log(`import.meta`, import.meta)
console.log(`import.meta.env`, import.meta.env)
// console.log(`process.env`, process.env);

// import { getAssetPath } from './utils'

// console.log("Path", getAssetPath() );

const app = createApp(App)

// app.getAssetPath = getAssetPath;
app.use(createPinia())
app.use(router)

app.mount('#app')
