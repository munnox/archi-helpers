/** This is a Common utility library for better reuse */
import type { IntArchiModel, IntNode, IntLink, IntProfile } from './type_interfaces'

console.log(`import.meta.env`, import.meta.env)

/**
 * Uses the APP_VERSION env defined variable to figure out where the code is running
 * And set the return value to right path.
 *
 * @returns The correct path to where the local assets are being kept.
 */
export function getAssetPath(): string {
  if (__APP_VERSION__ == 'development') {
    console.log(`Vite app`)
    return import.meta.env.BASE_URL + 'src/assets'
  } else {
    console.log(`Prod app`)
    return ''
  }
}

export function getNodesByID(id: string, nodes: IntNode[]) {
  const found_node = []
  for (const node_ind in nodes) {
    const node: IntNode = nodes[node_ind]
    if (id == node.id) {
      // console.log(`Found node`, node);
      found_node.push(node)
    }
  }
  return found_node
}

export function getNodeLinks(node: IntNode, links: IntLink[]) {
  const links_in: IntLink[] = []
  const links_out: IntLink[] = []
  for (const link_ind in links) {
    const link: IntLink = links[link_ind]
    if (link.target == node.id) {
      // console.log(`Found target`, link);
      links_in.push(link)
    }
    if (link.source == node.id) {
      // console.log(`Found source`, link);
      links_out.push(link)
    }
  }
  return {
    in: links_in,
    out: links_out
  }
}

export function getSpecialisation(node: IntNode, profiles: IntProfile[]) {
  try {
    const found_specialisations = []
    for (const pro in profiles) {
      const profile = profiles[pro]
      if (profile.id == node.specialisation) {
        // console.log('Found Profile', profile);
        found_specialisations.push(profile)
      }
    }
    return found_specialisations
  } catch (error) {
    console.error('Error', error, node.specialisation)
  }
}

export function build_model(data_model: any) {
  const new_model: IntArchiModel = {
    nodes: data_model.nodes.map((node: IntNode) => {
      const new_node = node //getlinks(node, data_model.links);
      return new_node
    }),
    links: data_model.links,
    profiles: data_model.profiles
  }
  return new_model
}
