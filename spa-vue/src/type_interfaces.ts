/** This is a Common utility library for better reuse */

export interface IntProperty {
  key: string
  value: string
}

export interface IntElement {
  id: string
  name: string
  type: string
  specialisation: string | null
  documentation: string
  folder: string[]
  properties: IntProperty[]

  // constructor(id: string, name: string, type: string) {
  //     this.id = id;
  //     this.name = name;
  //     this.type = type;
  //     this.specialisation = null;
  // }
}

export interface IntLink extends IntElement {
  source: string
  target: string
}

export interface IntNode extends IntElement {
  links_in: IntLink[]
  links_out: IntLink[]

  // constructor(id: string, name: string, type: string) {
  //     this.id = id;
  //     this.name = name;
  //     this.type = type;
  //     this.specialisation = null;
  // }
}

export interface IntProfile {
  id: string
  name: string
  concept_type: string
  image_path: string
}

export interface IntArchiModel {
  nodes: IntNode[]
  links: IntLink[]
  profiles: IntProfile[]
}
